﻿using System;
using System.IO;                        // for FileSystemWatcher class
using System.Threading.Tasks;           // for Task class
using AgentConfigurationSerializerLibrary;

namespace ZabbixAgentService
{
    class Perfmon_module
    {
        Logger logger;
        IConfigurationProvider config_provider;
        Configuration config;

        bool perfmon_working;
        string path_to_report;
        string path_to_report_directory;

        public Perfmon_module(Logger _logger, IConfigurationProvider _config_provider, Configuration _config)
        {
            perfmon_working = false;
            logger = _logger;
            config_provider = _config_provider;
            config = _config;

            config_provider.OnChanged += new EventHandler<Configuration>(on_config_file_changed_event_for_perfmon_module);
        }

        public void on_config_file_changed_event_for_perfmon_module(object _sender, Configuration _updated_config)
        {
            config = _updated_config;
            logger.log_path = config.log_file_path;
        }

        public void start_monitoring()
        {
            if (perfmon_working == false)
            {
                perfmon_working = true;
                logger.log("   Perfmon module: Perfmon monitoring started");

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C logman start " + config.perfmon_module_name;
                startInfo.Verb = "runas";
                process.StartInfo = startInfo;
                process.Start();
                Task task_report_directory = listen_to_changes_in_report_directory();
            }
        }

        private async Task listen_to_changes_in_report_directory()
        {
            await start_watcher_on_report_directory();
            logger.log("  Perfmon module: Report directory created");

            Task task_report_file = listen_to_changes_in_report_file();
        }

        private Task start_watcher_on_report_directory()
        {
            var tcs = new TaskCompletionSource<bool>();

            path_to_report_directory = config.perfmon_path_reports + @"\" + config.perfmon_module_name;

            FileSystemWatcher watcher = new FileSystemWatcher(path_to_report_directory);
            FileSystemEventHandler createdHandler = null;

            createdHandler = (s, e) =>
            {
                tcs.TrySetResult(true);
                watcher.Dispose();
            };

            watcher.Changed += createdHandler;
            watcher.EnableRaisingEvents = true;

            return tcs.Task;
        }

        private async Task listen_to_changes_in_report_file()
        {
            await start_watcher();
            logger.log("  Perfmon module: Report file created");

            try
            {
                //File.Copy(path_to_report, @"\\" + config.server + @"\reports\report.html", true);
            }
            catch (Exception ex)
            {
                logger.log("   Perfmon module: Error while sending report to Zabbix server. Error message:");
                logger.log(ex.ToString());
            }

            perfmon_working = false;
            logger.log("  Perfmon module: Report sent to Zabbix server");
        }

        private Task start_watcher()
        {
            var tcs = new TaskCompletionSource<bool>();

            String report_directory = get_last_modified_directory();
            path_to_report = config.perfmon_path_reports + @"\" + config.perfmon_module_name + @"\" + report_directory + @"\" + "report.html";
            logger.log("   Perfmon module: Looking for report in " + path_to_report);

            FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(path_to_report));
            FileSystemEventHandler createdHandler = null;

            createdHandler = (s, e) =>
            {
                if (e.Name == Path.GetFileName(path_to_report))
                {
                    tcs.TrySetResult(true);
                    watcher.Changed -= createdHandler;
                    watcher.Dispose();
                }
            };

            watcher.Changed += createdHandler;
            watcher.EnableRaisingEvents = true;

            return tcs.Task;
        }

        private String get_last_modified_directory()
        {
            String latest_directory = "";

            try
            {
                string target_dir = string.Format(config.perfmon_path_reports + @"\" + config.perfmon_module_name);
                var directories_list = new DirectoryInfo(target_dir).GetDirectories();
                DateTime last_updated = DateTime.MinValue;
                
                foreach (DirectoryInfo directory in directories_list)
                {
                    if(directory.LastWriteTime > last_updated)
                    {
                        last_updated = directory.LastWriteTime;
                        latest_directory = directory.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Perfmon module: Error in function get_last_modified_directory. Error message: ");
                logger.log(ex.ToString());
            }

            return latest_directory;
        }
    }

}