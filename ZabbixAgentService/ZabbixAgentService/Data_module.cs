﻿using System;
using System.Diagnostics;                   // for PerformanceCounter class
using System.Collections.Generic;           // for List class
using System.Threading;                     // for Thread class
using System.Timers;                        // for Timer class
using AgentConfigurationSerializerLibrary;

namespace ZabbixAgentService
{
    class Counter
    {
        public PerformanceCounter counter;
        public Data data_unit;
        public CounterConfiguration counter_configuration;
        public Analyzer_module analyzer;
        Logger logger;

        public Counter(Logger _logger, int _analyzers_queue_size, string _category, string _parameter, string _key, int _id, string _type_of_threshold, float _threshold, int _number_of_values_to_exceed_threshold, string _instance_name = "N")
        {
            // _category - category of Performance Counter (Memory, Processor...)
            // _parameter - name of parameter
            // _id - parameter's id in Zabbix database
            // _threshold - if parameter's value is over (or under) threshold analyzer module collects detailed data (every 100ms)  
            // _type_of_threshold - indicator if value has to be > or < than threshold to be collected as detailed data
            // _config - optional parameter for some of Performance Counters constructors; instance name

            data_unit = new Data();
            counter_configuration = new CounterConfiguration(_category, _parameter, _key, _type_of_threshold, _threshold, _number_of_values_to_exceed_threshold, _instance_name);
            data_unit.name = _parameter;
            analyzer = new Analyzer_module(_category, _parameter, _threshold, _type_of_threshold, _analyzers_queue_size, _number_of_values_to_exceed_threshold);
            data_unit.id = _id;
            logger = _logger;

            try
            {
                if (_instance_name == "N")
                    counter = new PerformanceCounter(counter_configuration.category, counter_configuration.parameter);
                else
                    counter = new PerformanceCounter(counter_configuration.category, counter_configuration.parameter, _instance_name);
            }
            catch (Exception ex)
            {
                logger.log("Error while creating counter.");
                logger.log(ex.ToString());
            }
            
        }
    }

    public class Data_module
    {
        public Logger logger;
        Configuration config;
        IConfigurationProvider config_provider;

        private Database_module database_module;
        private Perfmon_module perfmon_module;
        private Sender_module sender_module;

        private static System.Timers.Timer data_module_timer;
        private Thread thread_data_module;
        public ManualResetEventSlim reset_event_for_data_module_thread;

        private System.Timers.Timer analyzer_module_timer;
        private Thread thread_analyzer_module;
        public ManualResetEventSlim reset_event_for_analyzer_module_thread;

        private List<Counter> list_of_counters;
        private int host_id;
        private int perfmon_trigger_id;
        private int sender_counter;
        private int analyzers_queue_size;
        private int sender_frequency;
        private static object _locker = new object();

        public Data_module(IConfigurationProvider _config_provider, Logger _logger, Database_module _database_module, Configuration _config)
        {
            logger = _logger;
            database_module = _database_module;
            config_provider = _config_provider;
            config = _config;

            list_of_counters = new List<Counter>();
            perfmon_module = new Perfmon_module(logger, config_provider, config);
            sender_module = new Sender_module(database_module, logger);
            perfmon_trigger_id = database_module.get_item_id("Trigger: Perfmon started");
            sender_counter = 0;
            host_id = database_module.host_id;

            update_configuration();
            config_provider.OnChanged += new EventHandler<Configuration>(on_config_file_changed_event_for_data_module);

            // Thread initialization
            reset_event_for_data_module_thread = new ManualResetEventSlim(false);
            thread_data_module = new Thread(data_module_handler);
            logger.log("Starting data module thread");
            thread_data_module.Start();

            reset_event_for_analyzer_module_thread = new ManualResetEventSlim(false);
            thread_analyzer_module = new Thread(analyzer_module_handler);
            logger.log("Starting analyzer module thread");
            thread_analyzer_module.Start();
        }

        private void data_module_handler()
        {
            data_module_timer = new System.Timers.Timer();
            data_module_timer.Elapsed += new ElapsedEventHandler(on_data_module_timer_event);
            data_module_timer.Interval = config.data_module_timer_interval;
            data_module_timer.Enabled = true;
            
            reset_event_for_data_module_thread.Wait();
        }

        private void on_data_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            get_data();
        }

        private void analyzer_module_handler()
        {
            analyzer_module_timer = new System.Timers.Timer();
            analyzer_module_timer.Elapsed += new ElapsedEventHandler(on_analyzer_module_timer_event);
            analyzer_module_timer.Interval = config.analyzer_module_timer_interval;
            analyzer_module_timer.Enabled = true;

            reset_event_for_analyzer_module_thread.Wait();
        }

        private void on_analyzer_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            var hasLock = false;
            try
            {
                Monitor.TryEnter(_locker, ref hasLock);
                if (!hasLock)
                {
                    return;
                }

                analyze_data();
                if (sender_counter >= sender_frequency)
                {
                    sender_module.send_data_to_zabbix_db();
                    sender_counter = 0;
                }
                else
                    sender_counter++;
            }
            finally
            {
                if (hasLock)
                {
                    Monitor.Exit(_locker);
                }
            }
            
        }

        public void get_data()
        {
            foreach (var counter in list_of_counters)
            {
                counter.data_unit.value = counter.counter.NextValue();
                counter.data_unit.clock = DateTime.Now;
            }

            foreach (var counter in list_of_counters)
                counter.analyzer.put_value_in_queue(counter.data_unit);
        }

        public void analyze_data()
        {
            logger.log("     Data module: Data analyzis started.");
            List<Data> all_data_to_send = new List<Data>();
            List<Data> temp_list = new List<Data>();

            // Trigger starting perfmon monitoring
            bool perfmon_monitoring_trigger = false;

            foreach (var counter in list_of_counters)
            {
                counter.analyzer.get_value_from_queue();
                temp_list = counter.analyzer.analyze(ref perfmon_monitoring_trigger);
                all_data_to_send.AddRange(temp_list);
                counter.analyzer.data_to_analyze.Clear();

                if (perfmon_monitoring_trigger == true)
                {
                    perfmon_module.start_monitoring();

                    // Temp data to start Zabbix trigger
                    Data trigger_tag = new Data();
                    trigger_tag.id = perfmon_trigger_id;
                    trigger_tag.name = "Perfmon trigger";
                    trigger_tag.value = 10000;
                    trigger_tag.timestamp = 0;
                    trigger_tag.ns = 0;
                    all_data_to_send.Add(trigger_tag);
                }
            }

            database_module.insert_into_sqlite_database_in_bulk(all_data_to_send); 
            logger.log("     Data module: Data analyzis finished. Records sent to local db = " + all_data_to_send.Count);
        }

        public void restart_data()
        {
            foreach (var counter in list_of_counters)
            {
                counter.analyzer.data_to_analyze.Clear();
                counter.analyzer.data_queue.Clear();
            }
        }

        public void on_config_file_changed_event_for_data_module(object _sender, Configuration _updated_config)
        {
            config = _updated_config;
            update_configuration();
        }

        public void update_configuration()
        {
            sender_frequency = (config.sender_module_timer_interval / config.analyzer_module_timer_interval) - 1;
            analyzers_queue_size = config.analyzer_module_timer_interval / config.data_module_timer_interval;
            logger.log_path = config.log_file_path;
            update_counter_configuration();
        }

        public void update_counter_configuration()
        {
            List<Counter> counters_from_config_file = new List<Counter>();
            foreach (CounterConfiguration counter_config in config.list_of_counters)
            {
                Counter temp = new Counter(logger, analyzers_queue_size, counter_config.category, counter_config.parameter, counter_config.key, 0, counter_config.type_of_threshold, counter_config.threshold, counter_config.number_of_values_to_exceed_threshold, counter_config.instance_name);
                counters_from_config_file.Add(temp);
            }
            try
            {
                list_of_counters.Clear();
                foreach (var counter in counters_from_config_file)
                {
                    bool counter_is_in_db = database_module.check_if_counter_is_in_db(counter.counter_configuration.key, host_id);
                    int new_item_id = 0;

                    if (counter_is_in_db == false)
                        new_item_id = database_module.add_new_counters_to_db(counter.counter_configuration.parameter, counter.counter_configuration.key, host_id);
                    else
                        new_item_id = database_module.get_item_id(counter.counter_configuration.key);

                    Counter temp;
                    PerformanceCounterCategory temp_category = new PerformanceCounterCategory(counter.counter_configuration.category);

                    // Category Type == 0 when category is of SingleInstance type
                    if (temp_category.CategoryType == 0)
                    {
                        temp = new Counter(logger, analyzers_queue_size, counter.counter_configuration.category, counter.counter_configuration.parameter, counter.counter_configuration.key, new_item_id, counter.counter_configuration.type_of_threshold, counter.counter_configuration.threshold, counter.counter_configuration.number_of_values_to_exceed_threshold, counter.counter_configuration.instance_name);
                    }
                    else
                    {
                        temp = new Counter(logger, analyzers_queue_size, counter.counter_configuration.category, counter.counter_configuration.parameter, counter.counter_configuration.key, new_item_id, counter.counter_configuration.type_of_threshold, counter.counter_configuration.threshold, counter.counter_configuration.number_of_values_to_exceed_threshold, counter.counter_configuration.instance_name);
                    }

                    list_of_counters.Add(temp);
                }
            }
            catch (Exception ex)
            {
                logger.log("Data module: Error in function update_configuration. Error message: ");
                logger.log(ex.ToString());
            }
        } 
    }
}
