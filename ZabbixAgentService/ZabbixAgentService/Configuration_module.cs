﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AgentConfigurationSerializerLibrary;

namespace ZabbixAgentService
{
    public interface IConfigurationProvider
    {
        event EventHandler<Configuration> OnChanged;
    }

    public class Configuration_module : IConfigurationProvider
    {
        Logger logger;

        public string config_file_path;
        public string connection_string;
        public int sender_frequency;

        public Configuration config;
        AgentSerializer agent_serializer;
        public event EventHandler<Configuration> OnChanged;

        public Configuration_module(Logger _logger)
        {
            config_file_path = System.AppDomain.CurrentDomain.BaseDirectory + @"Agent Configuration App\config_file.xml";
            //config_file_path = @"B:\ZabbixAgentAsService\config_file.xml";

            logger = _logger;
            config = new Configuration();
            agent_serializer = new AgentSerializer(config_file_path);

            configuration_module_handler();
            load_config_file();
        }

        private void configuration_module_handler()
        {
            Task task = listen_to_changes_in_config_file();
        }

        private async Task listen_to_changes_in_config_file()
        {
            await start_watcher();
            logger.log("Configuration file changed");
            System.Threading.Thread.Sleep(100);
            load_config_file();

            OnChanged.Invoke(this, this.config);
            configuration_module_handler();
        }

        private Task start_watcher()
        {
            var tcs = new TaskCompletionSource<bool>();

            FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(config_file_path));
            FileSystemEventHandler changedHandler = null;

            changedHandler = (s, e) =>
            {
                if (e.Name == Path.GetFileName(config_file_path))
                {
                    tcs.TrySetResult(true);
                    watcher.Changed -= changedHandler;
                    watcher.Dispose();
                }
            };

            watcher.Changed += changedHandler;
            watcher.EnableRaisingEvents = true;

            return tcs.Task;
        }

        private void load_config_file()
        {
            try
            {
                config = agent_serializer.read_from_config_file();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            sender_frequency = (config.sender_module_timer_interval / config.analyzer_module_timer_interval) - 1;
            connection_string = "SERVER=" + config.server + "; PORT = " + config.port_mysql + " ;" + "DATABASE=" + config.database + ";" + "UID=" + config.uid + ";" + "PASSWORD=" + config.password + ";";
            logger.log_path = config.log_file_path;
        }
    }
}
