﻿using System;
using System.Data.SQLite;                   // for SQLite connection
using MySql.Data.MySqlClient;               // for MySQL connection
using System.Collections.Generic;           // for List class  
using System.Globalization;                 // for NumberFormatInfo
using System.Threading;                     // for Thread class
using System.Threading.Tasks;               // for Task class
using System.Transactions;                  // for TransactionScope class
using AgentConfigurationSerializerLibrary;

namespace ZabbixAgentService
{
    public class SelectInformation : DatabaseElement
    {
        public string table;
        public string select;
        public string parameter;
        public string condition;
        public string second_parameter;
        public string second_condition;
        public string math_operator;

        public SelectInformation(string _table, string _select, string _parameter, string _math_operator, string _condition, string _second_parameter = "", string _second_condition = "")
        {
            table = _table;
            select = _select;
            parameter = _parameter;
            condition = _condition;
            math_operator = _math_operator;
            second_parameter = _second_parameter;
            second_condition = _second_condition;
        }

        public override string accept(VisitorInterface visitor)
        {
            string result = visitor.visit(this);
            return result;
        }
    }

    public class Database_module
    {
        Logger logger;
        CancellationToken token_sql_connections;
        IConfigurationProvider config_provider;
        Configuration config;

        private string sql;
        private string sqlite_database_name;

        private MySQLInsertVisitor mysql_insert_visitor;
        private SQLiteInsertVisitor sqlite_insert_visitor;
        private SelectVisitor mysql_select_visitor;
        private DeleteVisitor delete_visitor;

        public int host_id;
        private string connection_string;

        // Constructor for creating local db file
        public Database_module(string db_name, Logger _logger, IConfigurationProvider _config_provider, Configuration _config, CancellationToken _token_sql_connections)
        {
            logger = _logger;
            config = _config;
            token_sql_connections = _token_sql_connections;
            config_provider = _config_provider;

            update_configuration();
            config_provider.OnChanged += new EventHandler<Configuration>(on_config_file_changed_event_for_database_module);
            try
            {
                SQLiteConnection.CreateFile(db_name);
                sqlite_database_name = "Data Source=LocalDB.sqlite;Version=3;";
                SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name);
            }
            catch (Exception ex)
            {
                logger.log(ex.ToString());
            }

            sql = "";

            // Visitors initialization
            mysql_insert_visitor = new MySQLInsertVisitor();
            sqlite_insert_visitor = new SQLiteInsertVisitor();
            mysql_select_visitor = new SelectVisitor();
            delete_visitor = new DeleteVisitor();

            test_mysql_connection();
            create_database();

            // Host creation
            if (check_if_host_exists() == false)
                register_host_in_db();

            host_id = get_host_id();
        }

        // Constructor for operations on existing local db file
        public Database_module(Logger _logger, IConfigurationProvider _config_provider, Configuration _config, CancellationToken _token_sql_connections)
        {
            logger = _logger;
            config = _config;

            token_sql_connections = _token_sql_connections;
            config_provider = _config_provider;

            update_configuration();
            config_provider.OnChanged += new EventHandler<Configuration>(on_config_file_changed_event_for_database_module);

            sqlite_database_name = "Data Source=LocalDB.sqlite;Version=3;";
            SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name);
            sql = "";

            // Visitors initialization
            mysql_insert_visitor = new MySQLInsertVisitor();
            sqlite_insert_visitor = new SQLiteInsertVisitor();
            mysql_select_visitor = new SelectVisitor();
            delete_visitor = new DeleteVisitor();

            // Host creation
            if (check_if_host_exists() == false)
                register_host_in_db();

            host_id = get_host_id();
        }

        public void on_config_file_changed_event_for_database_module(object _sender, Configuration _updated_config)
        {
            config = _updated_config;
            update_configuration();
        }

        public void update_configuration()
        {
            connection_string = "SERVER=" + config.server + "; PORT = " + config.port_mysql + " ;" + "DATABASE=" + config.database + ";" + "UID=" + config.uid + ";" + "PASSWORD=" + config.password + ";";
            logger.log_path = config.log_file_path;
        }

        // Creating databases data and processes
        public void create_database()
        {
            using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
            {
                db_connection.Open();
                sql = create_sqlite_query_create_database();

                using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        // Testing MySQL database connection at the start of the program
        public void test_mysql_connection()
        {
            MySqlConnection db_test_connection = new MySqlConnection(connection_string);
            db_test_connection.Open();
            logger.log("Connection to MySQL database OK.");
            db_test_connection.Close();
        }

        // Finding host id in Mysql database having hostname
        public int get_host_id()
        {
            int host_id = 0;
            SelectInformation information_object = new SelectInformation("hosts", "hostid", "host", "=", config.hostname);
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                host_id = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_host_id. Error message: ");
                logger.log(ex.ToString());
            }
            return host_id;
        }

        // Finding parameter id having hostname and parameter name
        public int get_item_id(string _key)
        {
            int id = 0;
            SelectInformation information_object = new SelectInformation("items", "*", "hostid", "=", host_id.ToString(), " AND key_ = '", _key += "'");
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                id = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_item_id. Error message: ");
                logger.log(ex.ToString());
            }
            return id;
        }

        // Inserting new processes to local db  
        public void insert_into_sqlite_database_in_bulk(List<_Process> _list_of_processes)
        {
            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (var transaction = db_connection.BeginTransaction())
                    {
                        foreach (var process in _list_of_processes)
                        {
                            _ProcessClass process_class_object = new _ProcessClass(process);
                            sql = process_class_object.accept(sqlite_insert_visitor);
                            using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                            {
                                command.ExecuteNonQuery();
                            }
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function insert_into_sqlite_database_in_bulk for processes. Error message: ");
                logger.log(ex.ToString());
            }
        }

        // Inserting new data to local db  
        public void insert_into_sqlite_database_in_bulk(List<Data> _data_to_analyze)
        {
            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (var transaction = db_connection.BeginTransaction())
                    {
                        foreach (var data in _data_to_analyze)
                        {
                            DataClass data_class_object = new DataClass(data, "");
                            sql = data_class_object.accept(sqlite_insert_visitor);
                            using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                            {
                                command.ExecuteNonQuery();
                            }
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function insert_into_sqlite_database_in_bulk for data. Error message: ");
                logger.log(ex.ToString());
            }
        }

        // Checking if current process (_id, _name) is already in local db
        public bool find_process_in_db(int _id, string _name)
        {
            SelectInformation information_object = new SelectInformation("processes", "*", "name", "=", _name, " AND id = ", _id.ToString());
            sql = information_object.accept(mysql_select_visitor);
            int temp_timestamp = 1;
            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                temp_timestamp = reader.GetInt32(4);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function find_process_in_db. Error message: ");
                logger.log(ex.ToString());
            }

            // temp_timestamp equals zero only if the process is in db AND is unfinished; function returns true, so the process is not added again
            // temp_timestamp does not equal zero when there is no such process in db OR there is finished one; function returns false, so the process is added to db
            if (temp_timestamp == 0)
                return true;
            else
                return false;
        }

        // Selecting all unfinished processes from local db (timestamp_finish = 0)
        public List<_Process> select_current_processes_from_db()
        {
            List<_Process> processes_from_db = new List<_Process>();
            SelectInformation information_object = new SelectInformation("processes", "*", "timestamp_finish", "=", "0");
            sql = information_object.accept(mysql_select_visitor);

            _Process temp_process = new _Process();
            temp_process.timestamp_finish = 0;
            temp_process.ns_finish = 0;

            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp_process.id = reader.GetInt32(0);
                                temp_process.name = reader.GetString(1);
                                temp_process.timestamp_start = reader.GetInt32(2);
                                temp_process.ns_start = reader.GetInt32(3);
                                processes_from_db.Add(temp_process);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function select_current_processes_from_db. Error message: ");
                logger.log(ex.ToString());
            }
            return processes_from_db;
        }

        // Setting process timestamp_finish to current time
        public void update_process_finish_time(_Process _process_to_update, int _timestamp, int _ns)
        {
            sql = create_sqlite_query_update_table_processes(_timestamp, _ns, _process_to_update.id, _process_to_update.name, _process_to_update.timestamp_start);
            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function update_process_finish_time. Error message: ");
                logger.log(ex.ToString());
            }
        }

        // Selecting all data from local db to send
        public List<Data> select_data_from_sqlite_database()
        {
            List<Data> data_from_db = new List<Data>();
            Data data_unit = new Data();
            sql = create_sqlite_query_select_from_table_data();

            using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
            {
                db_connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                {
                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            data_unit.id = reader.GetInt32(0);
                            try
                            {
                                string temp = reader.GetString(1);
                                data_unit.value = float.Parse(temp);
                            }
                            catch (Exception ex)
                            {
                                double temp2 = reader.GetDouble(1);
                                data_unit.value = Convert.ToSingle(temp2);
                                //logger.log(ex.ToString());
                            }
                            data_unit.timestamp = reader.GetInt32(2);
                            data_unit.ns = reader.GetInt32(3);
                            data_from_db.Add(data_unit);
                        }
                    }
                }
            }

            return data_from_db;
        }

        // Selecting all processes from local db to send
        public List<_Process> select_processes_from_sqlite_database(bool _clear_processes)
        {
            List<_Process> processes_from_db = new List<_Process>();
            _Process temp_process = new _Process();
            SelectInformation information_object;
            if (_clear_processes == false)
                information_object = new SelectInformation("processes", "*", "timestamp_finish", " != ", "0");
            else
            {
                information_object = new SelectInformation("processes", "*", "timestamp_start", " != ", "0");
                logger.log("Database module: Selecting all processes from local db.");
            }

            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp_process.id = reader.GetInt32(0);
                                temp_process.name = reader.GetString(1);
                                temp_process.timestamp_start = reader.GetInt32(2);
                                temp_process.ns_start = reader.GetInt32(3);
                                temp_process.timestamp_finish = reader.GetInt32(4);
                                temp_process.ns_finish = reader.GetInt32(5);
                                processes_from_db.Add(temp_process);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function select_processes_from_sqlite_database. Error message:");
                logger.log(ex.ToString());
            }
            logger.log("Database module: Selected " + processes_from_db.Count + " processes");
            return processes_from_db;
        }

        public void delete_all_processes_from_local_db()
        {
            sql = create_sqlite_query_delete_all_processes();
            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function delete_all_processes_from_local_db. Error message: ");
                logger.log(ex.ToString());
            }
        }

        public void delete_data_from_local_db()
        {
            DataClass data_class_object = new DataClass();
            _ProcessClass process_class_object = new _ProcessClass();
            sql = data_class_object.accept(delete_visitor);

            try
            {
                using (SQLiteConnection db_connection = new SQLiteConnection(sqlite_database_name))
                {
                    db_connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    sql = process_class_object.accept(delete_visitor);
                    using (SQLiteCommand command = new SQLiteCommand(sql, db_connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function delete_data_from_local_db. Error message: ");
                logger.log(ex.ToString());
            }
        }

        public async Task<bool> insert_into_mysql_database(List<Data> _data_from_local_db, List<_Process> _processes_from_local_db)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                    {
                        db_connection_mysql.Open();
                        logger.log("Database module: Sending " + _data_from_local_db.Count + " data records, " + _processes_from_local_db.Count + " process records.");

                        NumberFormatInfo nfi = new NumberFormatInfo();
                        nfi.NumberDecimalSeparator = ".";

                        MySqlCommand command;

                        foreach (var data in _data_from_local_db)
                        {
                            DataClass data_class_object = new DataClass(data, data.value.ToString(nfi));
                            sql = data_class_object.accept(mysql_insert_visitor);

                            command = new MySqlCommand(sql, db_connection_mysql);
                            await command.ExecuteNonQueryAsync(token_sql_connections);
                        }

                        foreach (var process in _processes_from_local_db)
                        {
                            _ProcessClass process_class_object = new _ProcessClass(process);
                            sql = process_class_object.accept(mysql_insert_visitor);

                            command = new MySqlCommand(sql, db_connection_mysql);
                            await command.ExecuteNonQueryAsync(token_sql_connections);
                        }
                    }

                    delete_data_from_local_db();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function insert_into_mysql_database. Error message: ");
                logger.log(ex.ToString());
            }
            logger.log("Database module: Data sent to MySQL database");
            return true;
        }

        public bool check_if_host_exists()
        {
            bool host_exists;
            int host_id = 0;


            SelectInformation information_object = new SelectInformation("hosts", "hostid", "host", "=", config.hostname);
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                host_id = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function check_if_host_exists. Error message: ");
                logger.log(ex.ToString());
            }

            if (host_id == 0)
            {
                logger.log("Database module: Creating new host");
                host_exists = false;
            }
            else
            {
                logger.log("Database module: Host " + config.hostname + " exists");
                host_exists = true;
            }

            return host_exists;
        }

        public void register_host_in_db()
        {
            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();

                    int template_id = get_host_template_id(db_connection_mysql, config.template);
                    int next_host_id = get_next_host_id(db_connection_mysql);
                    int next_host_template_id = get_next_host_template_id(db_connection_mysql);
                    int next_item_id = get_next_item_id(db_connection_mysql);

                    sql = create_mysql_query_insert_into_table_hosts(next_host_id, config.hostname);
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                        command.ExecuteNonQuery();

                    sql = create_mysql_query_insert_into_table_hosts_templates(next_host_template_id, next_host_id, template_id);
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                        command.ExecuteNonQuery();

                    List<String> names = new List<String>();
                    List<String> keys = new List<String>();

                    SelectInformation information_object = new SelectInformation("items", "name,key_", "hostid", "=", template_id.ToString());
                    sql = information_object.accept(mysql_select_visitor);

                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                names.Add(reader.GetString(0));
                                keys.Add(reader.GetString(1));
                            }
                        }
                    }

                    for (int i = 0; i < names.Count; i++)
                    {
                        add_item(db_connection_mysql, next_item_id, next_host_id, names[i], keys[i]);
                        next_item_id++;
                    }

                    next_host_id++;
                    next_host_template_id++;

                    sql = create_mysql_query_update_table_ids(next_host_id, next_host_template_id, next_item_id);
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                        command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function register_host. Error message: ");
                logger.log(ex.ToString());
            }
        }

        private int get_host_template_id(MySqlConnection _db_connection_mysql, string _template)
        {
            int template_id = 0;
            SelectInformation information_object = new SelectInformation("hosts", "hostid", "host", "=", _template);
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlCommand command = new MySqlCommand(sql, _db_connection_mysql))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            template_id = reader.GetInt32(0);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_host_template_id. Error message: ");
                logger.log(ex.ToString());
            }
            return template_id;
        }

        private int get_next_host_id(MySqlConnection _db_connection_mysql)
        {
            int next_host_id = 0;
            SelectInformation information_object = new SelectInformation("ids", "nextid", "field_name", "=", "hostid");
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlCommand command = new MySqlCommand(sql, _db_connection_mysql))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            next_host_id = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_next_host_id. Error message: ");
                logger.log(ex.ToString());
            }
            return next_host_id;
        }

        private int get_next_host_template_id(MySqlConnection _db_connection_mysql)
        {
            int next_host_template_id = 0;
            SelectInformation information_object = new SelectInformation("ids", "nextid", "field_name", "=", "hosttemplateid");
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlCommand command = new MySqlCommand(sql, _db_connection_mysql))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            next_host_template_id = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_next_host_template_id. Error message: ");
                logger.log(ex.ToString());
            }
            return next_host_template_id;
        }

        private int get_next_item_id(MySqlConnection _db_connection_mysql)
        {
            int next_item_id = 0;
            SelectInformation information_object = new SelectInformation("ids", "nextid", "field_name", "=", "itemid");
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlCommand command = new MySqlCommand(sql, _db_connection_mysql))
                {
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            next_item_id = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function get_next_item_id. Error message: ");
                logger.log(ex.ToString());
            }
            return next_item_id;
        }

        private void add_item(MySqlConnection _db_connection_mysql, int _next_item_id, int _next_host_id, string item_name, string item_key)
        {
            try
            {
                sql = create_mysql_query_insert_into_table_items(_next_item_id, _next_host_id, item_name, item_key);
                using (MySqlCommand command = new MySqlCommand(sql, _db_connection_mysql))
                    command.ExecuteNonQuery();

                logger.log("Database module: New item added");
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function add_item. Item: " + item_name + ", key: " + item_key + ", item_id: " + _next_item_id + ". Error message: ");
                logger.log(ex.ToString());
            }
        }

        public int add_new_counters_to_db(string _counter_name, string _counter_key, int _host_id)
        {
            int next_item_id = 0;

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    next_item_id = get_next_item_id(db_connection_mysql);
                    add_item(db_connection_mysql, next_item_id, _host_id, _counter_name, _counter_key);
                    next_item_id++;

                    sql = create_mysql_query_update_table_ids_nextitemid(next_item_id);
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                        command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function add_new_counters_to_db. Error message: ");
                logger.log(ex.ToString());
            }

            next_item_id--;
            return next_item_id;
        }

        public bool check_if_counter_is_in_db(string _key, int _host_id)
        {
            bool already_in_db = false;
            SelectInformation information_object = new SelectInformation("items", "itemid", "hostid", "=", _host_id.ToString(), " AND key_ = '", _key += "'");
            sql = information_object.accept(mysql_select_visitor);

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                already_in_db = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.log("Database module: Error in function check_if_counter_is_in_db. Error message:");
                logger.log(ex.ToString());
            }
            return already_in_db;
        }

        // SQL queries

        string create_mysql_query_insert_into_table_hosts(int _next_host_id, string _hostname)
        {
            string query = "INSERT INTO `hosts`(`hostid`, `proxy_hostid`, `host`, `status`, `disable_until`, `error`, `available`, `errors_from`, `lastaccess`, `ipmi_authtype`, `ipmi_privilege`, `ipmi_username`, `ipmi_password`, `ipmi_disable_until`, `ipmi_available`, `snmp_disable_until`, `snmp_available`, `maintenanceid`, `maintenance_status`, `maintenance_type`, `maintenance_from`, `ipmi_errors_from`, `snmp_errors_from`, `ipmi_error`, `snmp_error`, `jmx_disable_until`, `jmx_available`, `jmx_errors_from`, `jmx_error`, `name`, `flags`, `templateid`, `description`, `tls_connect`, `tls_accept`, `tls_issuer`, `tls_subject`, `tls_psk_identity`, `tls_psk`) VALUES (" + _next_host_id + ",null,'" + _hostname + "',0,0,'',0,0,0,0,0,'','',0,0,0,0,null,0,0,0,0,0,'','',0,0,0,'','" + _hostname + "',0,null,'',0,0,'','','','')";
            return query;
        }

        string create_mysql_query_insert_into_table_hosts_templates(int _next_host_template_id, int _next_host_id, int _template_id)
        {
            string query = "INSERT INTO `hosts_templates`(`hosttemplateid`, `hostid`, `templateid`) VALUES (" + _next_host_template_id + "," + _next_host_id + "," + _template_id + ")";
            return query;
        }

        string create_mysql_query_update_table_ids(int _next_host_id, int _next_host_template_id, int _next_item_id)
        {
            string query = "UPDATE `ids` SET `nextid`=" + _next_host_id + " WHERE `field_name`='hostid'; UPDATE `ids` SET `nextid`=" + _next_host_template_id + " WHERE `field_name`='hosttemplateid'; UPDATE `ids` SET `nextid`=" + _next_item_id + " WHERE `field_name`='itemid'";
            return query;
        }

        string create_mysql_query_insert_into_table_items(int _next_item_id, int _next_host_id, string _item_name, string _item_key)
        {
            string query = "INSERT INTO `items`(`itemid`, `type`, `snmp_community`, `snmp_oid`, `hostid`, `name`, `key_`, `delay`, `history`, `trends`, `status`, `value_type`, `trapper_hosts`, `units`, `multiplier`, `delta`, `snmpv3_securityname`, `snmpv3_securitylevel`, `snmpv3_authpassphrase`, `snmpv3_privpassphrase`, `formula`, `error`, `lastlogsize`, `logtimefmt`, `templateid`, `valuemapid`, `delay_flex`, `params`, `ipmi_sensor`, `data_type`, `authtype`, `username`, `password`, `publickey`, `privatekey`, `mtime`, `flags`, `interfaceid`, `port`, `description`, `inventory_link`, `lifetime`, `snmpv3_authprotocol`, `snmpv3_privprotocol`, `state`, `snmpv3_contextname`, `evaltype`) VALUES(" + _next_item_id + ", 2, '', '', " + _next_host_id + ", '" + _item_name + "', '" + _item_key + "', '1m', '365d', '354d', 0, 3, '', '', 0, 0, '', 0, '', '', '', '', 0, '', null, null, '', '', '', 0, 0, '', '', '', '', 0, 0, null, '', '', 0, '', 0, 0, 0, '', 0)";
            return query;
        }

        string create_mysql_query_update_table_ids_nextitemid(int _next_item_id)
        {
            string query = "UPDATE `ids` SET `nextid`=" + _next_item_id + " WHERE `field_name`='itemid'";
            return query;
        }

        string create_sqlite_query_create_database()
        {
            string query = "create table data (id int, item varchar(20), value float, timestamp int, ns int); create table processes (id int, name varchar(20), timestamp_start int, ns_start int, timestamp_finish int, ns_finish int)";
            return query;
        }

        string create_sqlite_query_update_table_processes(int _timestamp, int _ns, int _id, string _name, int _timestamp_start)
        {
            string query = "UPDATE processes set timestamp_finish = " + _timestamp + ", ns_finish = " + _ns + " where id = " + _id + " AND name = '" + _name + "' AND timestamp_start = " + _timestamp_start;
            return query;
        }

        string create_sqlite_query_select_from_table_data()
        {
            string query = "SELECT id, value, timestamp, ns FROM data";
            return query;
        }

        string create_sqlite_query_count_processes()
        {
            string query = "SELECT COUNT(id) FROM processes";
            return query;
        }

        string create_sqlite_query_delete_all_processes()
        {
            string query = "DELETE FROM processes";
            return query;
        }
    }

    public abstract class DatabaseElement
    {
        public abstract string accept(VisitorInterface visitor);
    }

    public abstract class VisitorInterface
    {
        public abstract string visit(DataClass _database_element);
        public abstract string visit(_ProcessClass _database_element);
        public abstract string visit(SelectInformation _database_element);
    }

    public class MySQLInsertVisitor : VisitorInterface
    {
        public override string visit(DataClass _database_element)
        {
            string query = "INSERT INTO `history`(`itemid`, `clock`, `value`, `ns`) VALUES (" + _database_element.data_item.id + ", " + _database_element.data_item.timestamp + ", '" + _database_element.value_as_string + "', " + _database_element.data_item.ns + ")";
            return query;
        }

        public override string visit(_ProcessClass _database_element)
        {
            string query = "INSERT INTO `processes`(`id`, `name`, `timestamp_start`, `ns_start`, `timestamp_finish`, `ns_finish`) VALUES (" + _database_element.proc.id + ", '" + _database_element.proc.name + "', " + _database_element.proc.timestamp_start + ", " + _database_element.proc.ns_start + ", " + _database_element.proc.timestamp_finish + ", " + _database_element.proc.ns_finish + ")";
            return query;
        }

        public override string visit(SelectInformation _database_element)
        {
            string query = "";
            return query;
        }
    }

    public class SQLiteInsertVisitor : VisitorInterface
    {
        public override string visit(DataClass _database_element)
        {
            string query = "insert into data (id, item, value, timestamp, ns) values (" + _database_element.data_item.id + ", '" + _database_element.data_item.name + "', '" + _database_element.data_item.value + "', " + _database_element.data_item.timestamp + ", " + _database_element.data_item.ns + ")";
            return query;
        }

        public override string visit(_ProcessClass _database_element)
        {
            string query = "insert into processes (id, name, timestamp_start, ns_start, timestamp_finish, ns_finish) values (" + _database_element.proc.id + ", '" + _database_element.proc.name + "', " + _database_element.proc.timestamp_start + ", " + _database_element.proc.ns_start + ", 0, 0)";
            return query;
        }

        public override string visit(SelectInformation _database_element)
        {
            string query = "";
            return query;
        }
    }

    public class SelectVisitor : VisitorInterface
    {
        public override string visit(DataClass _database_element)
        {
            string query = "";
            return query;
        }

        public override string visit(_ProcessClass _database_element)
        {
            string query = "";
            return query;
        }

        public override string visit(SelectInformation _database_element)
        {
            string query = "SELECT " + _database_element.select + " FROM `" + _database_element.table + "` WHERE " + _database_element.parameter + _database_element.math_operator + "'" + _database_element.condition + "'" + _database_element.second_parameter + _database_element.second_condition;
            return query;
        }
    }

    public class DeleteVisitor : VisitorInterface
    {
        public override string visit(DataClass _database_element)
        {
            string query = "DELETE FROM data";
            return query;
        }

        public override string visit(_ProcessClass _database_element)
        {
            string query = "DELETE FROM processes where timestamp_finish != 0";
            return query;
        }

        public override string visit(SelectInformation _database_element)
        {
            string query = "";
            return query;
        }
    }
}


