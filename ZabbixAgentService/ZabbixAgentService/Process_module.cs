﻿using System;
using System.Diagnostics;                   // for Process class
using System.Collections.Generic;           // for List class
using System.Threading;                     // for Thread class
using System.Timers;                        // for Timer class
using System.Linq;
using AgentConfigurationSerializerLibrary;

namespace ZabbixAgentService
{
    public class _ProcessClass : DatabaseElement
    {
        public _Process proc;

        public _ProcessClass(_Process _process)
        {
            proc = _process;
        }

        public _ProcessClass()
        { }

        public override string accept(VisitorInterface visitor)
        {
            string result = visitor.visit(this);
            return result;
        }
    }

    public struct _Process
    {
        public string name;
        public int id;
        public int timestamp_start;
        public int ns_start;
        public int timestamp_finish;
        public int ns_finish;
    }

    public class Process_module
    {
        Logger logger;
        IConfigurationProvider config_provider;
        Configuration config;

        public Database_module database_module;

        private Process[] table_of_processes;           // temporary storage for list of processes
        private List<_Process> list_of_processes;       // current system processes 
        public List<_Process> processes_to_send;       // list after analyzis; contains new processes that will be added to local db
        private List<_Process> current_db_processes;    // processes listed as current (not finished) in local database
        private List<_Process> new_processes;
        private List<_Process> new_processes_with_timestamp;
        private List<_Process> finished_processes;

        private Dictionary<Tuple<int, string>, Tuple<int, int>> processes_with_timestamps;

        private System.Timers.Timer process_module_timer;
        private Thread thread_process_module;
        public ManualResetEventSlim reset_event_for_process_module_thread;

        public Process_module(Logger _logger, IConfigurationProvider _config_provider, Database_module _database_module, Configuration _config, string _connection_string, CancellationToken _token_sql_connections)
        {
            //
            processes_with_timestamps = new Dictionary<Tuple<int, string>, Tuple<int, int>>();
            //

            logger = _logger;
            database_module = new Database_module(_logger, _config_provider, _config, _token_sql_connections);
            config_provider = _config_provider;
            config = _config;

            config_provider.OnChanged += new EventHandler<Configuration>(on_config_file_changed_event_for_process_module);

            list_of_processes = new List<_Process>();
            finished_processes = new List<_Process>();
            processes_to_send = new List<_Process>();
            current_db_processes = new List<_Process>();
            new_processes = new List<_Process>();

            // Thread initialization
            reset_event_for_process_module_thread = new ManualResetEventSlim(false);
            thread_process_module = new Thread(process_module_handler);
            logger.log("Starting process module thread");
            thread_process_module.Start();
        }

        public void on_config_file_changed_event_for_process_module(object _sender, Configuration _updated_config)
        {
            config = _updated_config;
            logger.log_path = config.log_file_path;
        }

        private void process_module_handler()
        {
            process_module_timer = new System.Timers.Timer();
            process_module_timer.Elapsed += new ElapsedEventHandler(on_process_module_timer_event);
            process_module_timer.Interval = config.process_module_timer_interval;
            process_module_timer.Enabled = true;

            reset_event_for_process_module_thread.Wait();
        }

        private void on_process_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                get_process_list();
                analyze_list();
            }
            catch (Exception ex)
            {
                logger.log("on_process_module_timer_event");
                logger.log(ex.ToString());
            }
            
        }

        public void get_process_list()
        {
            table_of_processes = Process.GetProcesses();
            DateTime time_now = DateTime.Now;
            int temp_timestamp = calculate_timestamp(time_now);
            int temp_ns = calculate_nanoseconds(time_now);
            _Process temp_process = new _Process();

            foreach (Process single_process in table_of_processes)
            {
                temp_process.name = single_process.ProcessName;
                temp_process.id = single_process.Id;
                list_of_processes.Add(temp_process);

                var t = new Tuple<int, string>(temp_process.id, temp_process.name);
                if (!processes_with_timestamps.ContainsKey(t))
                    processes_with_timestamps.Add(t, new Tuple<int, int>(temp_timestamp, temp_ns));
            }
        }

        public void analyze_list()
        {
            _Process temp_process = new _Process();
            DateTime time_now = DateTime.Now;
            int temp_timestamp = calculate_timestamp(time_now);
            int temp_ns = calculate_nanoseconds(time_now);

            new_processes = list_of_processes.Except(current_db_processes).ToList();
            finished_processes = current_db_processes.Except(list_of_processes).ToList();

            current_db_processes.AddRange(new_processes);
            current_db_processes = current_db_processes.Except(finished_processes).ToList();

            for (int i = 0; i < finished_processes.Count; i++)
            {
                temp_process.name = finished_processes[i].name;
                temp_process.id = finished_processes[i].id;
                var temp_tuple_key = new Tuple<int, string>(finished_processes[i].id, finished_processes[i].name);
                var temp_tuple_value = new Tuple<int, int>(0, 0);
                temp_tuple_value = processes_with_timestamps[temp_tuple_key];

                temp_process.timestamp_start = temp_tuple_value.Item1;
                temp_process.ns_start = temp_tuple_value.Item2;
                temp_process.timestamp_finish = temp_timestamp;
                temp_process.ns_finish = temp_ns;
                processes_to_send.Add(temp_process);
                processes_with_timestamps.Remove(temp_tuple_key);
            }

            list_of_processes.Clear();
            finished_processes.Clear();

        }

        private int calculate_timestamp(DateTime _clock)
        {
            long timestamp = ((DateTimeOffset)_clock).ToUnixTimeSeconds();
            return unchecked((int)timestamp);
        }

        private int calculate_nanoseconds(DateTime _clock)
        {
            return _clock.Millisecond * 1000;
        }
    }
}
