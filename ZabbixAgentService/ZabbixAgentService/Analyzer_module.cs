﻿using System;
using System.Collections.Generic;   // for List and Queue classes

namespace ZabbixAgentService
{
    public class DataClass : DatabaseElement
    {
        public Data data_item;
        public string value_as_string;

        public DataClass(Data _data_item, string _value)
        {
            data_item = _data_item;
            value_as_string = _value;
        }

        public DataClass()
        { }

        public override string accept(VisitorInterface visitor)
        {
            string result = visitor.visit(this);
            return result;
        }
    }

    public struct Data
    {
        public int id;
        public string name;
        public float value;
        public int timestamp;
        public int ns;
        public DateTime clock;
    }

    class Analyzer_module
    {
        public List<Data> data_to_analyze;
        public Queue<Data> data_queue;
        private float threshold;
        private string type_of_threshold;
        private string parameter;
        private int size_of_queue;
        private int number_of_values_to_exceed_threshold;

        public Analyzer_module(string _category, string _parameter, float _threshold, string _type_of_threshold, int _analyzers_queue_size, int _number_of_values_to_exceed_threshold)
        {
            data_queue = new Queue<Data>();
            data_to_analyze = new List<Data>();
            threshold = _threshold;
            type_of_threshold = _type_of_threshold;
            parameter = _parameter;
            size_of_queue = _analyzers_queue_size;
            number_of_values_to_exceed_threshold = _number_of_values_to_exceed_threshold;
        }

        public void put_value_in_queue(Data data_unit)
        {
            data_unit.timestamp = calculate_timestamp(data_unit.clock);
            data_unit.ns = calculate_nanoseconds(data_unit.clock);
            data_queue.Enqueue(data_unit);
        }

        public void get_value_from_queue()
        {
            if (data_queue.Count >= size_of_queue)
            {
                for (int i = 0; i < size_of_queue; i++)
                    data_to_analyze.Add(data_queue.Dequeue());
            }
        }

        public List<Data> analyze(ref bool perfmon_monitoring_trigger)
        {
            bool counter_generates_trigger = false;
            float sum = 0;
            List<Data> data_to_send = new List<Data>();
            int alarm_counter = 0;

            if (data_to_analyze.Count >= size_of_queue)
            {
                for (int i = 0; i < size_of_queue; i++)
                {
                    if (type_of_threshold == ">")
                    {
                        if (data_to_analyze[i].value > threshold)
                        {
                            //counter_generates_trigger = true;
                            alarm_counter++;
                            //break;
                        }
                    }
                    if (type_of_threshold == "<")
                    {
                        if (data_to_analyze[i].value < threshold)
                        {
                            //counter_generates_trigger = true;
                            alarm_counter++;
                            //break;
                        }
                    }
                    sum += data_to_analyze[i].value;
                }
                float avg = sum / size_of_queue;

                if(alarm_counter > number_of_values_to_exceed_threshold)
                    counter_generates_trigger = true;

                if (counter_generates_trigger == false)
                {
                    data_to_send.Clear();
                    Data data_unit = new Data();
                    data_unit.name = data_to_analyze[0].name;
                    data_unit.id = data_to_analyze[0].id;
                    data_unit.value = avg;
                    data_unit.timestamp = data_to_analyze[0].timestamp;
                    data_unit.ns = data_to_analyze[0].ns;
                    data_to_send.Add(data_unit);
                    return (data_to_send);
                }
                else
                {
                    perfmon_monitoring_trigger = true;
                    return (data_to_analyze);
                }
            }
            return data_to_send;
        }

        private int calculate_timestamp(DateTime _clock)
        {
            long timestamp = ((DateTimeOffset)_clock).ToUnixTimeSeconds();
            return unchecked((int)timestamp);
        }

        private int calculate_nanoseconds(DateTime _clock)
        {
            return _clock.Millisecond * 1000;
        }

        public void update_configuration(float _new_threshold_value, string _new_threshold_type)
        {
            threshold = _new_threshold_value;
            type_of_threshold = _new_threshold_type;
        }
    }
}

