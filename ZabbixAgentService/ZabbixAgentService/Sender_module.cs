﻿using System.Collections.Generic;       // for List class
using System.Text;                      // for Encoding function
using System.Threading.Tasks;           // for Task class


namespace ZabbixAgentService
{
    class Sender_module
    {
        Logger logger;

        Database_module database_module;
        List<Data> data_from_local_db;
        List<_Process> processes_from_local_db;

        int process_limit;

        int server_port = 10051;
        bool zabbix_trigger;

        public Sender_module(Database_module _database_module, Logger _logger)
        {
            logger = _logger;
            database_module = _database_module;

            data_from_local_db = new List<Data>();
            processes_from_local_db = new List<_Process>();
            zabbix_trigger = false;
            process_limit = 200;
        }

        public void get_data_from_local_db()
        {
            data_from_local_db = database_module.select_data_from_sqlite_database();
            processes_from_local_db.AddRange(Service1.process_module.processes_to_send);

            Service1.process_module.processes_to_send.Clear();

            // Zabbix triggers
            int index = 0;

            for (int i = data_from_local_db.Count - 1; i >= 0; i--)
            {
                if (data_from_local_db[i].timestamp == 0)
                {
                    zabbix_trigger = true;
                    index = i;
                    break;
                }
            }
            if (zabbix_trigger == true)
                data_from_local_db.RemoveAt(index);
        }

        public void send_data_to_zabbix_db()
        {
            get_data_from_local_db();
            Task<bool> send_data_task = database_module.insert_into_mysql_database(data_from_local_db, processes_from_local_db);

            processes_from_local_db.Clear();
        }
    }
}
