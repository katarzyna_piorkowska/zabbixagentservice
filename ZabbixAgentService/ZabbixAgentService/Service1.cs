﻿using System;
using System.Threading;         // for Thread class
using System.IO;                // for File class
using System.ServiceProcess;    // for ServiceBase class

namespace ZabbixAgentService
{
    public class Logger
    {
        public string log_path;

        public Logger(string _temp_path)
        {
            log_path = _temp_path;
        }

        public void log(string log_message)
        {
            log_message = log_message + Environment.NewLine;
            lock (this) { File.AppendAllText(log_path, log_message); }
        }
    }
    
    public partial class Service1 : ServiceBase
    {
        public static Logger logger;

        public static Data_module data_module;
        public static Process_module process_module;
        public static Configuration_module configuration_module;
        public static Database_module database_module;

        public static CancellationTokenSource token_source_sql_connections;
        private static CancellationToken token_sql_connections;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            initialize_logger();
            create_cancallation_tokens();
            try
            {
                if (configure_agent())
                {
                    database_module = new Database_module("LocalDB.sqlite", logger, configuration_module, configuration_module.config, token_sql_connections);
                    data_module = new Data_module(configuration_module, logger, database_module, configuration_module.config);
                    process_module = new Process_module(logger, configuration_module,  database_module, configuration_module.config, configuration_module.connection_string, token_sql_connections);
                }
            }
            catch (Exception ex)
            {
                logger.log("Error while configuring Zabbix agent.");
                logger.log(ex.ToString());
            }
        }

        private void initialize_logger()
        {
            //logger = new Logger(@"B:\ZabbixAgentAsService\Log\zabbix_agent.log");
            logger = new Logger(@"C:\Zabbix\zabbix_agent.log");
        }

        void create_cancallation_tokens()
        {
            token_source_sql_connections = new CancellationTokenSource();
            token_sql_connections = token_source_sql_connections.Token;
        }

        private bool configure_agent()
        {
            bool agent_configuration_ok = true;
            try
            {
                configuration_module = new Configuration_module(logger);
            }
            catch (Exception ex)
            {
                logger.log("No configuration file in Zabbix Agent exe location.");
                logger.log(ex.ToString());
                agent_configuration_ok = false;
            }
            return agent_configuration_ok;
        }

        protected override void OnStop()
        {
            token_source_sql_connections.Cancel();

            data_module.reset_event_for_data_module_thread.Set();
            data_module.reset_event_for_analyzer_module_thread.Set();
            process_module.reset_event_for_process_module_thread.Set();

            logger.log("Service stopped");
        }

        protected override void OnShutdown()
        {
            token_source_sql_connections.Cancel();

            data_module.reset_event_for_data_module_thread.Set();
            data_module.reset_event_for_analyzer_module_thread.Set();
            process_module.reset_event_for_process_module_thread.Set();

            logger.log("System shutdown");
        }

        protected override void OnPause()
        {
            logger.log("Service paused");
        }

        protected override void OnContinue()
        {
            logger.log("Service continued");
        }
    }
}

