# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % Hyperv physical disk
    if (M(i,1) == 24169)        % Current disk queue length
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24170)        % Avg disk sec/Read
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24171)        % Avg disk sec/Write
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 24190)        % % free space C:
        M_to_file2(b,5)=M(i,3);
    end 
    if (M(i,1) == 24191)        % % free space D:
        M_to_file2(b,6)=M(i,3);
    end 
    if (M(i,1) == 24294)     % Free megabytes C:
        M_to_file2(b,7)=M(i,3);
    end 
    if (M(i,1) == 24295)        % Free megabytes D:
        M_to_file2(b,8)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_hyperv_disk_physical.csv M_to_file2;

