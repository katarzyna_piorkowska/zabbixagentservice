# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % VM disk
    if (M(i,1) == 24202)        % % disk time 
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24242)        % % free space C:
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24244)        % Free megabytes C:
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 24207)        % Disk reads/sec
        M_to_file2(b,5)=M(i,3);
    end 
    if (M(i,1) == 24208)        % Disk writes/sec
        M_to_file2(b,6)=M(i,3);
    end 
    if (M(i,1) == 24215)        % Split I/O/sec
        M_to_file2(b,7)=M(i,3);
    end 
    if (M(i,1) == 24197)        % Avg disk sec/Read
        M_to_file2(b,8)=M(i,3);
    end 
    if (M(i,1) == 24198)        % Avg disk sec/Write
        M_to_file2(b,9)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_vm_bb.csv M_to_file2;

