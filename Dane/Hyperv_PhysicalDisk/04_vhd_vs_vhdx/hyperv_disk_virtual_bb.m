# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % Hyperv virtual disk
    if (M(i,1) ==  	24297)        % Maximum Bandwidth 
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24299)        % Latency
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24301)        % Lower latency
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 24303)        % Throughput
        M_to_file2(b,5)=M(i,3);
    end 
    if (M(i,1) == 24305)        % Queue Length
        M_to_file2(b,6)=M(i,3);
    end 
    if (M(i,1) == 24306)        % Write Operations/Sec
        M_to_file2(b,7)=M(i,3);
    end 
    if (M(i,1) == 24309)        % Read Operations/Sec
        M_to_file2(b,8)=M(i,3);
    end 
    if (M(i,1) == 24311)        % Write Bytes/sec 
        M_to_file2(b,9)=M(i,3);
    end 
    if (M(i,1) == 24313)        % Read Bytes/sec 
        M_to_file2(b,10)=M(i,3);
    end 
    if (M(i,1) == 24315)        % Error Count 
        M_to_file2(b,11)=M(i,3);
    end 
    if (M(i,1) == 24317)        % Flush Count 
        M_to_file2(b,12)=M(i,3);
    end 
    if (M(i,1) == 24319)        % Write Count 
        M_to_file2(b,13)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_hyperv_disk_virtual_bb.csv M_to_file2;

