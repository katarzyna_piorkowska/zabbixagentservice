# Host Test2 i Test3
filename = "history.csv";
M = csvread(filename);

DiskTime = [];
CurrentDiskQueueLength = [];
AvgDiskSecRead = [];
AvgDiskSecWrite = [];
DiskReadsSec = [];
DiskWritesSec = [];
DiskReadBytesSec = [];
DiskWriteBytesSec = [];

HypervDiskTime = [];
HypervCurrentDiskQueueLength = [];
HypervAvgDiskSecRead = [];
HypervAvgDiskSecWrite = [];
HypervDiskReadsSec = [];
HypervDiskWritesSec = [];
HypervDiskReadBytesSec = [];
HypervDiskWriteBytesSec = [];

for i=1:rows(M)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    if (M(i,1) == 23949)
        HypervDiskTime=[HypervDiskTime;M(i,:)];
    end
    if (M(i,1) == 23913)
        HypervCurrentDiskQueueLength=[HypervCurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 23911)
        HypervAvgDiskSecRead=[HypervAvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 23912)
        HypervAvgDiskSecWrite=[HypervAvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) == 23954)
        HypervDiskReadsSec=[HypervDiskReadsSec;M(i,:)];
    end
    if (M(i,1) == 23955)
        HypervDiskWritesSec=[HypervDiskWritesSec;M(i,:)];
    end
    if (M(i,1) == 23957)
        HypervDiskReadBytesSec=[HypervDiskReadBytesSec;M(i,:)];
    end
    if (M(i,1) == 23958)
        HypervDiskWriteBytesSec=[HypervDiskWriteBytesSec;M(i,:)];
    end
    %%%%%%%%%%%%%%%%%%
    if (M(i,1) == 23915)
        DiskTime=[DiskTime;M(i,:)];
    end
    if (M(i,1) == 23908)
        CurrentDiskQueueLength=[CurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 23906)
        AvgDiskSecRead=[AvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 23907)
        AvgDiskSecWrite=[AvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) == 23920)
        DiskReadsSec=[DiskReadsSec;M(i,:)];
    end
    if (M(i,1) == 23921)
        DiskWritesSec=[DiskWritesSec;M(i,:)];
    end
    if (M(i,1) == 23923)
        DiskReadBytesSec=[DiskReadBytesSec;M(i,:)];
    end
    if (M(i,1) == 23924)
        DiskWriteBytesSec=[DiskWriteBytesSec;M(i,:)];
    end
end

%plot(CurrentDiskQueueLength(:,2), CurrentDiskQueueLength(:,3))
%title ("CurrentDiskQueueLength");


plot(AvgDiskSecRead(:,2), AvgDiskSecRead(:,3), ':',  AvgDiskSecWrite(:,2),  AvgDiskSecWrite(:,3), '--')
title ("Avg. Disk Bytes");
legend('Read','Write');

%plot(DiskBytesSec(:,2), DiskBytesSec(:,3), '-', DiskReadBytesSec(:,2), DiskReadBytesSec(:,3), ':', DiskWriteBytesSec(:,2), DiskWriteBytesSec(:,3), '--')
%title ("Disk Bytes/sec");
%legend('Total','Read','Write');

%plot(DiskReadsSec(:,2), DiskReadsSec(:,3), ':', DiskWritesSec(:,2), DiskWritesSec(:,3), '--')
%title ("Disk operations/sec");
%legend('Reads','Writes');

%plot(HypervAvgDiskSecRead(:,2), HypervAvgDiskSecRead(:,3), ':', HypervAvgDiskSecWrite(:,2), HypervAvgDiskSecWrite(:,3), '--')
%title ("Hyperv Disk operations/sec");
%legend('Reads','Writes');

%plot(ProcessorTime(:,2), ProcessorTime(:,3), '-', UserTime(:,2), UserTime(:,3), ':', PrivilegedTime(:,2), PrivilegedTime(:,3), '--')
%title ("Processor");
%legend('Processor','User','Privileged');

