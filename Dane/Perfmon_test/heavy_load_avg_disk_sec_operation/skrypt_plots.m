# Host Test2
filename = "history.csv";
M = csvread(filename);

ProcessorTime = [];
UserTime = [];
PrivilegedTime = [];
InterruptsSec = [];
DPCTime = [];
InterruptTime = [];
DPCsQueuedSec = [];
DPCRate = [];
IdleTime = [];
C1Time = [];
C2Time = [];
C3Time = [];
C1TransitionsSec = [];
C2TransitionsSec = [];
C3TransitionsSec = [];

PageFaultsSec = [];
AvailableBytes = [];
CommitedBytes = [];
WriteCopiesSec = [];
TransitionFaultsSec = [];
CacheFaultsSec = [];
PagesSec = [];
PagesInputSec = [];
PagesOutputSec = [];
PageReadsSec = [];
PageWritesSec = [];
PoolPagedBytes = [];
PoolNonpagedBytes = [];
PoolPagedAllocs = [];
PoolNonpagedAllocs = [];
CacheBytes = [];

CurrentDiskQueueLength = [];
DiskTime = [];
DiskReadTime = [];
DiskWriteTime = [];
AvgDiskSecRead = [];
AvgDiskSecWrite = [];
DiskReadsSec = [];
DiskWritesSec = [];
DiskTransfersSec = [];
DiskBytesSec = [];
DiskReadBytesSec = [];
DiskWriteBytesSec = [];
AvgDiskBytesRead = [];
AvgDiskBytesWrite = [];

for i=1:rows(M)
    if (M(i,1) == 23909)
        ProcessorTime=[ProcessorTime;M(i,:)];
    end
    if (M(i,1) == 23929)
        UserTime=[UserTime;M(i,:)];
    end
    if (M(i,1) == 23930)
        PrivilegedTime=[PrivilegedTime;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (M(i,1) == 23936)
        PageFaultsSec=[PageFaultsSec;M(i,:)];
    end
    if (M(i,1) == 23905)
        AvailableBytes=[AvailableBytes;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (M(i,1) == 23915)
        DiskTime=[DiskTime;M(i,:)];
    end
    if (M(i,1) == 23908)
        CurrentDiskQueueLength=[CurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 23906)
        AvgDiskSecRead=[AvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 23907)
        AvgDiskSecWrite=[AvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) == 23920)
        DiskReadsSec=[DiskReadsSec;M(i,:)];
    end
    if (M(i,1) == 23921)
        DiskWritesSec=[DiskWritesSec;M(i,:)];
    end
    if (M(i,1) == 23923)
        DiskReadBytesSec=[DiskReadBytesSec;M(i,:)];
    end
    if (M(i,1) == 23924)
        DiskWriteBytesSec=[DiskWriteBytesSec;M(i,:)];
    end
end

%plot(AvailableBytes(:,2), AvailableBytes(:,3))
%title ("AvailableBytes");

plot(AvgDiskSecRead(:,2), AvgDiskSecRead(:,3), ':', AvgDiskSecWrite(:,2), AvgDiskSecWrite(:,3), '--')
title ("Avg. Disk Sec Operation");
legend('Read','Write');

%plot(DiskBytesSec(:,2), DiskBytesSec(:,3), '-', DiskReadBytesSec(:,2), DiskReadBytesSec(:,3), ':', DiskWriteBytesSec(:,2), DiskWriteBytesSec(:,3), '--')
%title ("Disk Bytes/sec");
%legend('Total','Read','Write');

%plot(DiskReadsSec(:,2), DiskReadsSec(:,3), ':', DiskWritesSec(:,2), DiskWritesSec(:,3), '--')
%title ("Disk operations/sec");
%legend('Reads','Writes');

%plot(ProcessorTime(:,2), ProcessorTime(:,3), '-', UserTime(:,2), UserTime(:,3), ':', PrivilegedTime(:,2), PrivilegedTime(:,3), '--')
%title ("Processor");
%legend('Processor','User','Privileged');

