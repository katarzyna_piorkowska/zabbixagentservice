# Host Hyperv_host
filename = "history3.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % CPU dispatch time
    if (M(i,1) == 24453)        % clo_has 0
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24452)        % clo_has 1
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24450)        % deb01 0
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 24449)        % deb01 1
        M_to_file2(b,5)=M(i,3);
    end 
    if (M(i,1) == 24448)        % deb02
        M_to_file2(b,6)=M(i,3);
    end 
    if (M(i,1) == 24451)        % hv manager
        M_to_file2(b,7)=M(i,3);
    end 
    if (M(i,1) == 24447)        % win_domain_test
        M_to_file2(b,8)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_hyperv_dispatch_time3.csv M_to_file2;

