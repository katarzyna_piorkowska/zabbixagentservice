# Host Hyperv_host
filename = "history3.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % Physical Memory
    if (M(i,1) == 24434)        % clo_has
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24431)        % deb01
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24432)        % deb02
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 24433)        % hvmanager
        M_to_file2(b,5)=M(i,3);
    end 
    if (M(i,1) == 24430)        % win_domain_test
        M_to_file2(b,6)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_hyperv_physical_memory3.csv M_to_file2;

