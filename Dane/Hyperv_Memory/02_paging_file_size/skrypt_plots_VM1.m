# Host Test2
filename = "history.csv";
M = csvread(filename);

ProcessorTime = [];
UserTime = [];
PrivilegedTime = [];
InterruptsSec = [];
DPCTime = [];
InterruptTime = [];
DPCsQueuedSec = [];
DPCRate = [];
IdleTime = [];
C1Time = [];
C2Time = [];
C3Time = [];
C1TransitionsSec = [];
C2TransitionsSec = [];
C3TransitionsSec = [];

PageFaultsSec = [];
AvailableBytes = [];
CommitedBytes = [];
WriteCopiesSec = [];
TransitionFaultsSec = [];
CacheFaultsSec = [];
PagesSec = [];
PagesInputSec = [];
PagesOutputSec = [];
PageReadsSec = [];
PageWritesSec = [];
PoolPagedBytes = [];
PoolNonpagedBytes = [];
PoolPagedAllocs = [];
PoolNonpagedAllocs = [];
CacheBytes = [];

CurrentDiskQueueLength = [];
DiskTime = [];
DiskReadTime = [];
DiskWriteTime = [];
AvgDiskSecRead = [];
AvgDiskSecWrite = [];
DiskReadsSec = [];
DiskWritesSec = [];
DiskTransfersSec = [];
DiskBytesSec = [];
DiskReadBytesSec = [];
DiskWriteBytesSec = [];
AvgDiskBytesRead = [];
AvgDiskBytesWrite = [];

for i=1:rows(M)
    if (M(i,1) == 24013)
        ProcessorTime=[ProcessorTime;M(i,:)];
    end
    if (M(i,1) == 24029)
        UserTime=[UserTime;M(i,:)];
    end
    if (M(i,1) == 24030)
        PrivilegedTime=[PrivilegedTime;M(i,:)];
    end
    if (M(i,1) == 24031)
        InterruptsSec=[InterruptsSec;M(i,:)];
    end
    if (M(i,1) == 24032)
        DPCTime=[DPCTime;M(i,:)];
    end
    if (M(i,1) == 24033)
        InterruptTime=[InterruptTime;M(i,:)];
    end
    if (M(i,1) == 24034)
        DPCsQueuedSec=[DPCsQueuedSec;M(i,:)];
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (M(i,1) == 24037)
        PageFaultsSec=[PageFaultsSec;M(i,:)];
    end
    if (M(i,1) == 24009)
        AvailableBytes=[AvailableBytes;M(i,:)];
    end
    if (M(i,1) == 24040)
        TransitionFaultsSec=[TransitionFaultsSec;M(i,:)];
    end
    if (M(i,1) == 24041)
        CacheFaultsSec=[CacheFaultsSec;M(i,:)];
    end
    if (M(i,1) == 24042)
        PagesSec=[PagesSec;M(i,:)];
    end
    if (M(i,1) == 24043)
        PagesInputSec=[PagesInputSec;M(i,:)];
    end
    if (M(i,1) == 24045)
        PagesOutputSec=[PagesOutputSec;M(i,:)];
    end
    if (M(i,1) == 24044)
        PageReadsSec=[PageReadsSec;M(i,:)];
    end
    if (M(i,1) == 24048)
        PageWritesSec=[PageWritesSec;M(i,:)];
    end
    if (M(i,1) == 24046)
        PoolPagedBytes=[PoolPagedBytes;M(i,:)];
    end
    if (M(i,1) == 24047)
        PoolNonpagedBytes=[PoolNonpagedBytes;M(i,:)];
    end
    if (M(i,1) == 24049)
        PoolPagedAllocs=[PoolPagedAllocs;M(i,:)];
    end
    if (M(i,1) == 24050)
        PoolNonpagedAllocs=[PoolNonpagedAllocs;M(i,:)];
    end
    if (M(i,1) == 24051)
        CacheBytes=[CacheBytes;M(i,:)];
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    if (M(i,1) == 24015)
        DiskTime=[DiskTime;M(i,:)];
    end
    if (M(i,1) == 24012)
        CurrentDiskQueueLength=[CurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 24010)
        AvgDiskSecRead=[AvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 24011)
        AvgDiskSecWrite=[AvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) ==  24020)
        DiskReadsSec=[DiskReadsSec;M(i,:)];
    end
    if (M(i,1) ==  24021)
        DiskWritesSec=[DiskWritesSec;M(i,:)];
    end
    if (M(i,1) == 24023)
        DiskReadBytesSec=[DiskReadBytesSec;M(i,:)];
    end
    if (M(i,1) == 24024)
        DiskWriteBytesSec=[DiskWriteBytesSec;M(i,:)];
    end
end

plot(PagesSec(:,2), PagesSec(:,3))
title ("PagesSec");

%plot(AvgDiskBytesTransfer(:,2), AvgDiskBytesTransfer(:,3), '-', AvgDiskBytesRead(:,2), AvgDiskBytesRead(:,3), ':', AvgDiskBytesWrite(:,2), AvgDiskBytesWrite(:,3), '--')
%title ("Avg. Disk Bytes");
%legend('Transfer','Read','Write');

%plot(DiskBytesSec(:,2), DiskBytesSec(:,3), '-', DiskReadBytesSec(:,2), DiskReadBytesSec(:,3), ':', DiskWriteBytesSec(:,2), DiskWriteBytesSec(:,3), '--')
%title ("Disk Bytes/sec");
%legend('Total','Read','Write');

%plot(DiskReadsSec(:,2), DiskReadsSec(:,3), ':', DiskWritesSec(:,2), DiskWritesSec(:,3), '--')
%title ("Disk operations/sec");
%legend('Reads','Writes');

%plot(ProcessorTime(:,2), ProcessorTime(:,3), '-', UserTime(:,2), UserTime(:,3), ':', PrivilegedTime(:,2), PrivilegedTime(:,3), '--')
%title ("Processor");
%legend('Processor','User','Privileged');

