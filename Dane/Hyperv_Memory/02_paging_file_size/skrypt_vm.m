# Host VM1
filename = "history.csv";
M = csvread(filename);
M_to_file = [];

b = 1;
for i=1:rows(M)

    % Memory
    if (M(i,1) == 24009)        % Available bytes
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end
    %if (M(i,1) == 24051)        % Cache bytes
    %    M_to_file(b,3)=M(i,3);
    %end  
    %if (M(i,1) == 24041)        % Cache faults/s
    %    M_to_file(b,4)=M(i,3);
    %end 
    if (M(i,1) == 24042)        % Pages/sec
        M_to_file(b,3)=M(i,3);
    end 
    if (M(i,1) == 24037)        % Page faults/sec
        M_to_file(b,4)=M(i,3);
    end 
    %if (M(i,1) == 24043)        % Pages Input/sec
    %    M_to_file(b,7)=M(i,3);
    %end 
    %if (M(i,1) == 24045)        % Pages Output/sec
    %    M_to_file(b,8)=M(i,3);
    %end 
    if (M(i,1) == 24044)        % Page Reads/sec
        M_to_file(b,5)=M(i,3);
        b = b + 1;
    end 
    %if (M(i,1) == 24048)        % Page Writes/sec
    %    M_to_file(b,10)=M(i,3);
    %end 
    %if (M(i,1) == 24046)        % Pool Paged Bytes
    %    M_to_file(b,11)=M(i,3);
    %end 
    %if (M(i,1) == 24047)        % Pool Nonpaged Bytes
    %    M_to_file(b,12)=M(i,3);
    %end 
    %if (M(i,1) == 24040)        % Transition faults/sec
    %    M_to_file(b,13)=M(i,3);
    %end 
    %if (M(i,1) == 24039)        % Write Copies/sec
    %    M_to_file(b,14)=M(i,3);
    %    b = b + 1;
    %end 
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_vm.csv M_to_file;

