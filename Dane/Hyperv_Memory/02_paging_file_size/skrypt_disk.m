# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file = [];

b = 1;
for i=1:rows(M)

    % Memory
    if (M(i,1) == 24176)        % Page file usage
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end
    if (M(i,1) == 24169)        % Current disk queue length
        M_to_file(b,3)=M(i,3);
    end  
    if (M(i,1) == 24170)        % Avg disk sec/Read
        M_to_file(b,4)=M(i,3);
    end 
    if (M(i,1) == 24171)        % Avg disk sec/Write
        M_to_file(b,5)=M(i,3);
    end 
    if (M(i,1) == 24172)        % Read Bytes/sec
        M_to_file(b,6)=M(i,3);
    end 
    if (M(i,1) == 24173)        % Write Bytes/sec
        M_to_file(b,7)=M(i,3);
    end 
    if (M(i,1) == 24174)        % Read Sectors/sec
        M_to_file(b,8)=M(i,3);
    end 
    if (M(i,1) == 24175)        % Written Sectors/sec
        M_to_file(b,9)=M(i,3);
    end 
    if (M(i,1) == 24168)        % Deposited Pages
        M_to_file(b,10)=M(i,3);
        b = b + 1;
    end 
    
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_disk.csv M_to_file;

