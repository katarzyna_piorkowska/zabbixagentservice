# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file = [];

b = 1;
for i=1:rows(M)

    % Memory
    if (M(i,1) == 24118)        % Balancer Available memory
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end
    if (M(i,1) == 24119)        % Balancer Average Pressure
        M_to_file(b,3)=M(i,3);
    end  
    if (M(i,1) == 24117)        % Balancer System Current Pressure
        M_to_file(b,4)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_hyperv_dynamic.csv M_to_file;

