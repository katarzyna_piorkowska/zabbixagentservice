# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file2 = [];

b = 1;
for i=1:rows(M)

    % Minimum Pressure
    if (M(i,1) == 24109)        % access_station
        M_to_file2(b,1)=M(i,2);
        M_to_file2(b,2)=M(i,3);
    end
    if (M(i,1) == 24110)        % openkm_test2
        M_to_file2(b,3)=M(i,3);
    end 
    if (M(i,1) == 24111)        % test_file_server
        M_to_file2(b,4)=M(i,3);
    end 
    if (M(i,1) == 2412)        % win10_mssql
        M_to_file2(b,5)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file2)
    M_to_file2(i,1)=a;
    a = a + 10;
end

save history_hyperv_min_pressure.csv M_to_file2;

