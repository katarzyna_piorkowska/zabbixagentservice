# Host Hyperv_host
filename = "history.csv";
M = csvread(filename);
M_to_file = [];

b = 1;
for i=1:rows(M)

    % Memory
    if (M(i,1) == 24063)        % Available bytes ok
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end
    if (M(i,1) == 24075)        % Cache bytes ok
        M_to_file(b,3)=M(i,3);
    end  
    if (M(i,1) == 24066)        % Cache faults/s ok
        M_to_file(b,4)=M(i,3);
    end 
    if (M(i,1) == 24068)        % Pages/sec ok
        M_to_file(b,5)=M(i,3);
    end 
    if (M(i,1) == 24062)        % Page faults/sec ok
        M_to_file(b,6)=M(i,3);
    end 
    if (M(i,1) == 24069)        % Pages Input/sec ok
        M_to_file(b,7)=M(i,3);
    end 
    if (M(i,1) == 24071)        % Pages Output/sec ok
        M_to_file(b,8)=M(i,3);
    end 
    if (M(i,1) == 24070)        % Page Reads/sec ok
        M_to_file(b,9)=M(i,3);
    end 
    if (M(i,1) == 24074)        % Page Writes/sec ok
        M_to_file(b,10)=M(i,3);
    end 
    if (M(i,1) == 24072)        % Pool Paged Bytes ok
        M_to_file(b,11)=M(i,3);
    end 
    if (M(i,1) == 24073)        % Pool Nonpaged Bytes ok
        M_to_file(b,12)=M(i,3);
    end 
    if (M(i,1) == 24065)        % Transition faults/sec ok
        M_to_file(b,13)=M(i,3);
    end 
    if (M(i,1) == 24064)        % Write Copies/sec ok
        M_to_file(b,14)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_hyperv.csv M_to_file;

