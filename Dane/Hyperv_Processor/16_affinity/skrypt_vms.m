# Host Test2
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file3 = [];

% Basic parameters
CurrentDiskQueueLength = [];
AvgDiskSecRead = [];
AvgDiskSecWrite = [];
ProcessorTime = [];
AvailableBytes = [];

% Logical processor
HypervGuestRunTime_LP_total = [];
HypervHypervisorRunTime_LP_total = [];
HypervGuestRunTime_LP_0 = [];
HypervHypervisorRunTime_LP_0 = [];
HypervGuestRunTime_LP_1 = [];
HypervHypervisorRunTime_LP_1 = [];
HypervGuestRunTime_LP_2 = [];
HypervHypervisorRunTime_LP_2 = [];
HypervGuestRunTime_LP_3 = [];
HypervHypervisorRunTime_LP_3 = [];
HypervGuestRunTime_LP_4 = [];
HypervHypervisorRunTime_LP_4 = [];
HypervGuestRunTime_LP_5 = [];
HypervHypervisorRunTime_LP_5 = [];
HypervGuestRunTime_LP_6 = [];
HypervHypervisorRunTime_LP_6 = [];
HypervGuestRunTime_LP_7 = [];
HypervHypervisorRunTime_LP_7 = [];
HypervTotalRunTime_LP = [];
HyperVTotalInterrupts_LP_total = [];
HyperVTotalInterrupts_LP_0 = [];
HyperVTotalInterrupts_LP_1 = [];
HyperVTotalInterrupts_LP_2 = [];
HyperVTotalInterrupts_LP_3 = [];
HyperVTotalInterrupts_LP_4 = [];
HyperVTotalInterrupts_LP_5 = [];
HyperVTotalInterrupts_LP_6 = [];
HyperVTotalInterrupts_LP_7 = [];
HypervHardwareInterrupts_LP = [];
HypervSchedulerInterrupts_LP = [];
HypervTimerInterrupts_LP = [];
HypervInterProcessorInterrupts_LP = [];
HypervContextSwitches_LP_total = [];
HypervContextSwitches_LP_0 = [];
HypervContextSwitches_LP_1 = [];
HypervContextSwitches_LP_2 = [];
HypervContextSwitches_LP_3 = [];
HypervContextSwitches_LP_4 = [];
HypervContextSwitches_LP_5 = [];
HypervContextSwitches_LP_6 = [];
HypervContextSwitches_LP_7 = [];
HypervMonitorTransitionCost_LP = [];

% Root partition processor
HypervGuestRunTime_Root_total = [];
HypervHypervisorRunTime_Root_total = [];
HypervGuestRunTime_Root_0 = [];
HypervHypervisorRunTime_Root_0 = [];
HypervGuestRunTime_Root_1 = [];
HypervHypervisorRunTime_Root_1 = [];
HypervGuestRunTime_Root_2 = [];
HypervHypervisorRunTime_Root_2 = [];
HypervGuestRunTime_Root_3 = [];
HypervHypervisorRunTime_Root_3 = [];
HypervGuestRunTime_Root_4 = [];
HypervHypervisorRunTime_Root_4 = [];
HypervGuestRunTime_Root_5 = [];
HypervHypervisorRunTime_Root_5 = [];
HypervGuestRunTime_Root_6 = [];
HypervHypervisorRunTime_Root_6 = [];
HypervGuestRunTime_Root_7 = [];
HypervHypervisorRunTime_Root_7 = [];
HypervTotalRunTime_Root = [];
HyperVHypercallsSec_Root = [];
HypervHypercallsCost_Root = [];
HypervInterceptsSec_Root = [];
HypervInterceptsCost_Root = [];

% Virtual processor
HypervGuestRunTime_VP_total = [];
HypervHypervisorRunTime_VP_total = [];
HypervGuestRunTime_VP_accessstation = [];
HypervHypervisorRunTime_VP_accessstation = [];
HypervGuestRunTime_VP_win10_mssql= [];
HypervHypervisorRunTime_VP_win10_mssql = [];
HypervGuestRunTime_VP_openkmtest2 = [];
HypervHypervisorRunTime_VP_openkmtest2 = [];
HypervGuestRunTime_VP_testfileserver = [];
HypervHypervisorRunTime_VP_testfileserver = [];
HypervGuestRunTime_VP_zabbixtest = [];
HypervHypervisorRunTime_VP_zabbixtest = [];
HypervTotalRunTime_VP = [];
HyperVHypercallsSec_VP = [];
HypervHypercallsCost_VP = [];
HypervInterceptsSec_VP = [];
HypervInterceptsCost_VP = [];

% Guest 1 (Test1): 10.10.200.76
Guest1ProcessorTime = [];
Guest1UserTime = [];
Guest1PrivilegedTime = [];
Guest1InterruptsSec = [];
Guest1DPCTime = [];
Guest1InterruptTime = [];
Guest1DPCRate = [];

% Guest 2 (Test2): 10.10.200.20
Guest2ProcessorTime = [];
Guest2UserTime = [];
Guest2PrivilegedTime = [];
Guest2InterruptsSec = [];
Guest2DPCTime = [];
Guest2InterruptTime = [];
Guest2DPCRate = [];

b = 1;
for i=1:rows(M)

%%%%%%%%%%%%%%%%%%%%  Basic parameters  %%%%%%%%%%%%%%%%%%%%%%  

    if (M(i,1) == 24199)
        CurrentDiskQueueLength=[CurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 24197)
        AvgDiskSecRead=[AvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 24198)
        AvgDiskSecWrite=[AvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) == 24200)
        ProcessorTime=[ProcessorTime;M(i,:)];
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    %  VM guest run time
    if (M(i,1) == 24029)          % VM user time
        M_to_file3(b,1)=M(i,2);    
        M_to_file3(b,2)=M(i,3);
    end  
    if (M(i,1) == 24030)          % VM privileged time
        M_to_file3(b,3)=M(i,3);    
    end  
    if (M(i,1) == 24052)          % process procesor time     
        M_to_file3(b,4)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file3)
    M_to_file3(i,1)=a;
    a = a + 10;
end

save temp_history_vm_processor_time.csv M_to_file3;


