# Host Test6
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file3 = [];
M_to_file6 = [];
M_to_file7 = [];

HypervDispatchTime_VP_total = [];
HypervDispatchTime_VP_accessstation = [];
HypervDispatchTime_VP_win10_mssql = [];

b = 1;
for i=1:rows(M)

%%%%%%%%%%%%%%%%%%%%  Basic parameters  %%%%%%%%%%%%%%%%%%%%%%  
    
% VP Dispatch Time
    if (M(i,1) == 24324)          % total
        M_to_file(b,1)=M(i,2);    
        M_to_file(b,2)=M(i,3);
    end  
    if (M(i,1) == 24350)          % root total
        M_to_file(b,3)=M(i,3);
    end 
    if (M(i,1) == 24325)          % access_station vp 0 
        M_to_file(b,4)=M(i,3);
    end
    if (M(i,1) == 24326)          % win10_mssql vp 0 
        M_to_file(b,5)=M(i,3);
    end 
    if (M(i,1) == 24336)          % openkm_test2
        M_to_file(b,6)=M(i,3);
    end 
    if (M(i,1) == 24334)          % test_file_server
        M_to_file(b,7)=M(i,3);
        b = b + 1;
    end 

end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save temp_history_hyperv_vp_dispatch_time_rest_of_vm.csv M_to_file;


