# Host Test2
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file3 = [];

% Basic parameters
CurrentDiskQueueLength = [];
AvgDiskSecRead = [];
AvgDiskSecWrite = [];
ProcessorTime = [];
AvailableBytes = [];

% Logical processor
HypervGuestRunTime_LP_total = [];
HypervHypervisorRunTime_LP_total = [];
HypervGuestRunTime_LP_0 = [];
HypervHypervisorRunTime_LP_0 = [];
HypervGuestRunTime_LP_1 = [];
HypervHypervisorRunTime_LP_1 = [];
HypervGuestRunTime_LP_2 = [];
HypervHypervisorRunTime_LP_2 = [];
HypervGuestRunTime_LP_3 = [];
HypervHypervisorRunTime_LP_3 = [];
HypervGuestRunTime_LP_4 = [];
HypervHypervisorRunTime_LP_4 = [];
HypervGuestRunTime_LP_5 = [];
HypervHypervisorRunTime_LP_5 = [];
HypervGuestRunTime_LP_6 = [];
HypervHypervisorRunTime_LP_6 = [];
HypervGuestRunTime_LP_7 = [];
HypervHypervisorRunTime_LP_7 = [];
HypervTotalRunTime_LP = [];
HyperVTotalInterrupts_LP_total = [];
HyperVTotalInterrupts_LP_0 = [];
HyperVTotalInterrupts_LP_1 = [];
HyperVTotalInterrupts_LP_2 = [];
HyperVTotalInterrupts_LP_3 = [];
HyperVTotalInterrupts_LP_4 = [];
HyperVTotalInterrupts_LP_5 = [];
HyperVTotalInterrupts_LP_6 = [];
HyperVTotalInterrupts_LP_7 = [];
HypervHardwareInterrupts_LP = [];
HypervSchedulerInterrupts_LP = [];
HypervTimerInterrupts_LP = [];
HypervInterProcessorInterrupts_LP = [];
HypervContextSwitches_LP_total = [];
HypervContextSwitches_LP_0 = [];
HypervContextSwitches_LP_1 = [];
HypervContextSwitches_LP_2 = [];
HypervContextSwitches_LP_3 = [];
HypervContextSwitches_LP_4 = [];
HypervContextSwitches_LP_5 = [];
HypervContextSwitches_LP_6 = [];
HypervContextSwitches_LP_7 = [];
HypervMonitorTransitionCost_LP = [];

% Root partition processor
HypervGuestRunTime_Root_total = [];
HypervHypervisorRunTime_Root_total = [];
HypervGuestRunTime_Root_0 = [];
HypervHypervisorRunTime_Root_0 = [];
HypervGuestRunTime_Root_1 = [];
HypervHypervisorRunTime_Root_1 = [];
HypervGuestRunTime_Root_2 = [];
HypervHypervisorRunTime_Root_2 = [];
HypervGuestRunTime_Root_3 = [];
HypervHypervisorRunTime_Root_3 = [];
HypervGuestRunTime_Root_4 = [];
HypervHypervisorRunTime_Root_4 = [];
HypervGuestRunTime_Root_5 = [];
HypervHypervisorRunTime_Root_5 = [];
HypervGuestRunTime_Root_6 = [];
HypervHypervisorRunTime_Root_6 = [];
HypervGuestRunTime_Root_7 = [];
HypervHypervisorRunTime_Root_7 = [];
HypervTotalRunTime_Root = [];
HyperVHypercallsSec_Root = [];
HypervHypercallsCost_Root = [];
HypervInterceptsSec_Root = [];
HypervInterceptsCost_Root = [];

% Virtual processor
HypervGuestRunTime_VP_total = [];
HypervHypervisorRunTime_VP_total = [];
HypervGuestRunTime_VP_accessstation = [];
HypervHypervisorRunTime_VP_accessstation = [];
HypervGuestRunTime_VP_win10_mssql= [];
HypervHypervisorRunTime_VP_win10_mssql = [];
HypervGuestRunTime_VP_openkmtest2 = [];
HypervHypervisorRunTime_VP_openkmtest2 = [];
HypervGuestRunTime_VP_testfileserver = [];
HypervHypervisorRunTime_VP_testfileserver = [];
HypervGuestRunTime_VP_zabbixtest = [];
HypervHypervisorRunTime_VP_zabbixtest = [];
HypervTotalRunTime_VP = [];
HyperVHypercallsSec_VP = [];
HypervHypercallsCost_VP = [];
HypervInterceptsSec_VP = [];
HypervInterceptsCost_VP = [];

% Guest 1 (Test1): 10.10.200.76
Guest1ProcessorTime = [];
Guest1UserTime = [];
Guest1PrivilegedTime = [];
Guest1InterruptsSec = [];
Guest1DPCTime = [];
Guest1InterruptTime = [];
Guest1DPCRate = [];

% Guest 2 (Test2): 10.10.200.20
Guest2ProcessorTime = [];
Guest2UserTime = [];
Guest2PrivilegedTime = [];
Guest2InterruptsSec = [];
Guest2DPCTime = [];
Guest2InterruptTime = [];
Guest2DPCRate = [];

b = 1;
for i=1:rows(M)

%%%%%%%%%%%%%%%%%%%%  Basic parameters  %%%%%%%%%%%%%%%%%%%%%%  

    if (M(i,1) == 24199)
        CurrentDiskQueueLength=[CurrentDiskQueueLength;M(i,:)];
    end
    if (M(i,1) == 24197)
        AvgDiskSecRead=[AvgDiskSecRead;M(i,:)];
    end
    if (M(i,1) == 24198)
        AvgDiskSecWrite=[AvgDiskSecWrite;M(i,:)];
    end
    if (M(i,1) == 24200)
        ProcessorTime=[ProcessorTime;M(i,:)];
    end

%%%%%%%%%%%%%%%%%%%%  Logical processor  %%%%%%%%%%%%%%%%%%%%%

    if (M(i,1) == 24242)
        HypervGuestRunTime_LP_total=[HypervGuestRunTime_LP_total;M(i,:)];
    end
    if (M(i,1) == 24251)
        HypervHypervisorRunTime_LP_total=[HypervHypervisorRunTime_LP_total;M(i,:)];
    end
    if (M(i,1) == 24243)
        HypervGuestRunTime_LP_0=[HypervGuestRunTime_LP_0;M(i,:)];
    end
    if (M(i,1) == 24252)
        HypervHypervisorRunTime_LP_0=[HypervHypervisorRunTime_LP_0;M(i,:)];
    end
    if (M(i,1) == 24244)
        HypervGuestRunTime_LP_1=[HypervGuestRunTime_LP_1;M(i,:)];
    end
    if (M(i,1) == 24253)
        HypervHypervisorRunTime_LP_1=[HypervHypervisorRunTime_LP_1;M(i,:)];
    end
    if (M(i,1) == 24245)
        HypervGuestRunTime_LP_2=[HypervGuestRunTime_LP_2;M(i,:)];
    end
    if (M(i,1) == 24254)
        HypervHypervisorRunTime_LP_2=[HypervHypervisorRunTime_LP_2;M(i,:)];
    end
    if (M(i,1) == 24246)
        HypervGuestRunTime_LP_3=[HypervGuestRunTime_LP_3;M(i,:)];
    end
    if (M(i,1) == 24255)
        HypervHypervisorRunTime_LP_3=[HypervHypervisorRunTime_LP_3;M(i,:)];
    end
    if (M(i,1) == 24247)
        HypervGuestRunTime_LP_4=[HypervGuestRunTime_LP_4;M(i,:)];
    end
    if (M(i,1) == 24256)
        HypervHypervisorRunTime_LP_4=[HypervHypervisorRunTime_LP_4;M(i,:)];
    end
    if (M(i,1) == 24248)
        HypervGuestRunTime_LP_5=[HypervGuestRunTime_LP_5;M(i,:)];
    end
    if (M(i,1) == 24257)
        HypervHypervisorRunTime_LP_5=[HypervHypervisorRunTime_LP_5;M(i,:)];
    end
    if (M(i,1) == 24249)
        HypervGuestRunTime_LP_6=[HypervGuestRunTime_LP_6;M(i,:)];
    end
    if (M(i,1) == 24258)
        HypervHypervisorRunTime_LP_6=[HypervHypervisorRunTime_LP_6;M(i,:)];
    end
    if (M(i,1) == 24250)
        HypervGuestRunTime_LP_7=[HypervGuestRunTime_LP_7;M(i,:)];
    end
    if (M(i,1) == 24259)
        HypervHypervisorRunTime_LP_7=[HypervHypervisorRunTime_LP_7;M(i,:)];
    end
    if (M(i,1) == 24260)
        HypervTotalRunTime_LP=[HypervTotalRunTime_LP;M(i,:)];
    end
    if (M(i,1) == 24261)
        HyperVTotalInterrupts_LP_total=[HyperVTotalInterrupts_LP_total;M(i,:)];
    end
    if (M(i,1) == 24262)
        HyperVTotalInterrupts_LP_0=[HyperVTotalInterrupts_LP_0;M(i,:)];
    end
    if (M(i,1) == 24263)
        HyperVTotalInterrupts_LP_1=[HyperVTotalInterrupts_LP_1;M(i,:)];
    end
    if (M(i,1) == 24264)
        HyperVTotalInterrupts_LP_2=[HyperVTotalInterrupts_LP_2;M(i,:)];
    end
    if (M(i,1) == 24265)
        HyperVTotalInterrupts_LP_3=[HyperVTotalInterrupts_LP_3;M(i,:)];
    end
    if (M(i,1) == 24266)
        HyperVTotalInterrupts_LP_4=[HyperVTotalInterrupts_LP_4;M(i,:)];
    end
    if (M(i,1) == 24267)
        HyperVTotalInterrupts_LP_5=[HyperVTotalInterrupts_LP_5;M(i,:)];
    end
    if (M(i,1) == 24268)
        HyperVTotalInterrupts_LP_6=[HyperVTotalInterrupts_LP_6;M(i,:)];
    end
    if (M(i,1) == 24269)
        HyperVTotalInterrupts_LP_7=[HyperVTotalInterrupts_LP_7;M(i,:)];
    end
    if (M(i,1) == 24270)
        HypervHardwareInterrupts_LP=[HypervHardwareInterrupts_LP;M(i,:)];
    end
    if (M(i,1) == 24271)
        HypervSchedulerInterrupts_LP=[HypervSchedulerInterrupts_LP;M(i,:)];
    end
    if (M(i,1) == 24272)
        HypervTimerInterrupts_LP=[HypervTimerInterrupts_LP;M(i,:)];
    end
    if (M(i,1) == 24273)
        HypervInterProcessorInterrupts_LP=[HypervInterProcessorInterrupts_LP;M(i,:)];
    end
    if (M(i,1) == 24275)
        HypervContextSwitches_LP_total=[HypervContextSwitches_LP_total;M(i,:)];
    end
    if (M(i,1) == 24276)
        HypervContextSwitches_LP_0=[HypervContextSwitches_LP_0;M(i,:)];
    end
    if (M(i,1) == 24277)
        HypervContextSwitches_LP_1=[HypervContextSwitches_LP_1;M(i,:)];
    end
    if (M(i,1) == 24278)
        HypervContextSwitches_LP_2=[HypervContextSwitches_LP_2;M(i,:)];
    end
    if (M(i,1) == 24279)
        HypervContextSwitches_LP_3=[HypervContextSwitches_LP_3;M(i,:)];
    end
    if (M(i,1) == 24280)
        HypervContextSwitches_LP_4=[HypervContextSwitches_LP_4;M(i,:)];
    end
    if (M(i,1) == 24281)
        HypervContextSwitches_LP_5=[HypervContextSwitches_LP_5;M(i,:)];
    end
    if (M(i,1) == 24282)
        HypervContextSwitches_LP_6=[HypervContextSwitches_LP_6;M(i,:)];
    end
    if (M(i,1) == 24283)
        HypervContextSwitches_LP_7=[HypervContextSwitches_LP_7;M(i,:)];
    end
    if (M(i,1) == 24274)
        HypervMonitorTransitionCost_LP=[HypervMonitorTransitionCost_LP;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%  Root partition processor  %%%%%%%%%%%%%%%%%%

    if (M(i,1) == 24219)
        HypervGuestRunTime_Root_total=[HypervGuestRunTime_Root_total;M(i,:)];
    end
    if (M(i,1) == 24228)
        HypervHypervisorRunTime_Root_total=[HypervHypervisorRunTime_Root_total;M(i,:)];
    end
    if (M(i,1) == 24220)
        HypervGuestRunTime_Root_0=[HypervGuestRunTime_Root_0;M(i,:)];
    end
    if (M(i,1) == 24229)
        HypervHypervisorRunTime_Root_0=[HypervHypervisorRunTime_Root_0;M(i,:)];
    end
    if (M(i,1) == 24221)
        HypervGuestRunTime_Root_1=[HypervGuestRunTime_Root_1;M(i,:)];
    end
    if (M(i,1) == 24230)
        HypervHypervisorRunTime_Root_1=[HypervHypervisorRunTime_Root_1;M(i,:)];
    end
    if (M(i,1) == 24222)
        HypervGuestRunTime_Root_2=[HypervGuestRunTime_Root_2;M(i,:)];
    end
    if (M(i,1) == 24231)
        HypervHypervisorRunTime_Root_2=[HypervHypervisorRunTime_Root_2;M(i,:)];
    end
    if (M(i,1) == 24223)
        HypervGuestRunTime_Root_3=[HypervGuestRunTime_Root_3;M(i,:)];
    end
    if (M(i,1) == 24232)
        HypervHypervisorRunTime_Root_3=[HypervHypervisorRunTime_Root_3;M(i,:)];
    end
    if (M(i,1) == 24224)
        HypervGuestRunTime_Root_4=[HypervGuestRunTime_Root_4;M(i,:)];
    end
    if (M(i,1) == 24233)
        HypervHypervisorRunTime_Root_4=[HypervHypervisorRunTime_Root_4;M(i,:)];
    end
    if (M(i,1) == 24225)
        HypervGuestRunTime_Root_5=[HypervGuestRunTime_Root_5;M(i,:)];
    end
    if (M(i,1) == 24234)
        HypervHypervisorRunTime_Root_5=[HypervHypervisorRunTime_Root_5;M(i,:)];
    end
    if (M(i,1) == 24226)
        HypervGuestRunTime_Root_6=[HypervGuestRunTime_Root_6;M(i,:)];
    end
    if (M(i,1) == 24235)
        HypervHypervisorRunTime_Root_6=[HypervHypervisorRunTime_Root_6;M(i,:)];
    end
    if (M(i,1) == 24227)
        HypervGuestRunTime_Root_7=[HypervGuestRunTime_Root_7;M(i,:)];
    end
    if (M(i,1) == 24236)
        HypervHypervisorRunTime_Root_7=[HypervHypervisorRunTime_Root_7;M(i,:)];
    end
    if (M(i,1) == 24237)
        HypervTotalRunTime_Root=[HypervTotalRunTime_Root;M(i,:)];
    end
    if (M(i,1) == 24238)
        HyperVHypercallsSec_Root=[HyperVHypercallsSec_Root;M(i,:)];
    end
    if (M(i,1) == 24239)
        HypervHypercallsCost_Root=[HypervHypercallsCost_Root;M(i,:)];
    end
    if (M(i,1) == 24241)
        HypervInterceptsSec_Root=[HypervInterceptsSec_Root;M(i,:)];
    end
    if (M(i,1) == 24240)
        HypervInterceptsCost_Root=[HypervInterceptsCost_Root;M(i,:)];
    end

%%%%%%%%%%%%%%%%%%%%  Virtual processor  %%%%%%%%%%%%%%%%%%%%%

    if (M(i,1) == 24202)
        HypervGuestRunTime_VP_total=[HypervGuestRunTime_VP_total;M(i,:)];
    end
    if (M(i,1) == 24208)
        HypervHypervisorRunTime_VP_total=[HypervHypervisorRunTime_VP_total;M(i,:)];
    end
    if (M(i,1) == 24203)
        HypervGuestRunTime_VP_accessstation=[HypervGuestRunTime_VP_accessstation;M(i,:)];
    end
    if (M(i,1) == 24209)
        HypervHypervisorRunTime_VP_accessstation=[HypervHypervisorRunTime_VP_accessstation;M(i,:)];
    end
    if (M(i,1) == 24322)
        HypervGuestRunTime_VP_win10_mssql=[HypervGuestRunTime_VP_win10_mssql;M(i,:)];
    end
    if (M(i,1) == 24323)
        HypervHypervisorRunTime_VP_win10_mssql=[HypervHypervisorRunTime_VP_win10_mssql;M(i,:)];
    end
    if (M(i,1) == 24204)
        HypervGuestRunTime_VP_openkmtest2=[HypervGuestRunTime_VP_openkmtest2;M(i,:)];
    end
    if (M(i,1) == 24210)
        HypervHypervisorRunTime_VP_openkmtest2=[HypervHypervisorRunTime_VP_openkmtest2;M(i,:)];
    end
    if (M(i,1) == 24205)
        HypervGuestRunTime_VP_testfileserver=[HypervGuestRunTime_VP_testfileserver;M(i,:)];
    end
    if (M(i,1) == 24211)
        HypervHypervisorRunTime_VP_testfileserver=[HypervHypervisorRunTime_VP_testfileserver;M(i,:)];
    end
    if (M(i,1) == 24207)
        HypervGuestRunTime_VP_zabbixtest=[HypervGuestRunTime_VP_zabbixtest;M(i,:)];
    end
    if (M(i,1) == 24213)
        HypervHypervisorRunTime_VP_zabbixtest=[HypervHypervisorRunTime_VP_zabbixtest;M(i,:)];
    end
    if (M(i,1) == 24214)
        HypervTotalRunTime_VP=[HypervTotalRunTime_VP;M(i,:)];
    end
    if (M(i,1) == 24215)
        HyperVHypercallsSec_VP=[HyperVHypercallsSec_VP;M(i,:)];
    end
    if (M(i,1) == 24216)
        HypervHypercallsCost_VP=[HypervHypercallsCost_VP;M(i,:)];
    end
    if (M(i,1) == 24218)
        HypervInterceptsSec_VP=[HypervInterceptsSec_VP;M(i,:)];
    end
    if (M(i,1) == 24217)
        HypervInterceptsCost_VP=[HypervInterceptsCost_VP;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%%%%%  Guest 1  %%%%%%%%%%%%%%%%%%%%%

    if (M(i,1) == 23904)
        Guest1ProcessorTime=[Guest1ProcessorTime;M(i,:)];
    end
    if (M(i,1) == 24299)
        Guest1UserTime=[Guest1UserTime;M(i,:)];
    end
    if (M(i,1) == 24300)
        Guest1PrivilegedTime=[Guest1PrivilegedTime;M(i,:)];
    end
    if (M(i,1) == 24301)
        Guest1InterruptsSec=[Guest1InterruptsSec;M(i,:)];
    end
    if (M(i,1) == 24302)
        Guest1DPCTime=[Guest1DPCTime;M(i,:)];
    end
    if (M(i,1) == 24303)
        Guest1InterruptTime=[Guest1InterruptTime;M(i,:)];
    end
    if (M(i,1) == 24305)
        Guest1DPCRate=[Guest1DPCRate;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%%%%%  Guest 2  %%%%%%%%%%%%%%%%%%%%%

    if (M(i,1) == 23909)
        Guest2ProcessorTime=[Guest2ProcessorTime;M(i,:)];
    end
    if (M(i,1) == 23929)
        Guest2UserTime=[Guest2UserTime;M(i,:)];
    end
    if (M(i,1) == 23930)
        Guest2PrivilegedTime=[Guest2PrivilegedTime;M(i,:)];
    end
    if (M(i,1) == 23931)
        Guest2InterruptsSec=[Guest2InterruptsSec;M(i,:)];
    end
    if (M(i,1) == 23932)
        Guest2DPCTime=[Guest2DPCTime;M(i,:)];
    end
    if (M(i,1) == 23933)
        Guest2InterruptTime=[Guest2InterruptTime;M(i,:)];
    end
    if (M(i,1) == 24038)
        Guest2DPCRate=[Guest2DPCRate;M(i,:)];
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #{ LP hypervisor run time
    if (M(i,1) == 24251)
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
        %b = b + 1;
    end
    if (M(i,1) == 24252)
        M_to_file(b,3)=M(i,3);
        %b = b + 1;
    end  
    if (M(i,1) == 24253)
        M_to_file(b,4)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24254)
        M_to_file(b,5)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24255)
        M_to_file(b,6)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24256)
        M_to_file(b,7)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24257)
        M_to_file(b,8)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24258)
        M_to_file(b,9)=M(i,3);
        %b = b + 1;
    end 
    if (M(i,1) == 24259)
        M_to_file(b,10)=M(i,3);
        %b = b + 1;
    end 
    #}
    
    % VP VM guest and hypervisor run time
    if (M(i,1) == 24322)          % win10_mssql guest
        M_to_file3(b,1)=M(i,2);    
        M_to_file3(b,2)=M(i,3);

    end  
    if (M(i,1) == 24323)          % win10_mssql hypervisor   
        M_to_file3(b,3)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file3)
    M_to_file3(i,1)=a;
    a = a + 10;
end

save history_libre.csv M_to_file3;


%X = [];
%Y1 = [];
%Y2 = [];

%X = M_to_file(:,1);
%Y1 = M_to_file(:,2);
%Y2 = M_to_file(:,3);

%save myfile1.csv X ;
%save myfile2.csv Y1;
%save myfile3.csv Y2;


