# Host Test6
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file4 = [];

b = 1;
for i=1:rows(M)

    % Bus parameters  
    if (M(i,1) == 24351)          % Bus Throttle Events
        M_to_file4(b,1)=M(i,2);
        M_to_file4(b,2)=M(i,3);
    end
    if (M(i,1) == 24352)          % Bus Interrupts Sent/sec
        M_to_file4(b,3)=M(i,3);
    end  
    if (M(i,1) == 24353)          % Bus Interrupts Received/sec
        M_to_file4(b,4)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file4)
    M_to_file4(i,1)=a;
    a = a + 10;
end

save history_hyperv_bus.csv M_to_file4;

