# Host Test6
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file4 = [];


b = 1;
for i=1:rows(M)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % LP interrupts, hypercalls, intercepts

    if (M(i,1) == 23833)              % total hypercalls
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end  
    if (M(i,1) == 23904)              % hardware interrupts 
        M_to_file(b,3)=M(i,3);
    end
    if (M(i,1) == 23905)              % scheduler interrupts 
        M_to_file(b,4)=M(i,3);
    end
    if (M(i,1) == 23906)              % timer interrupts 
        M_to_file(b,5)=M(i,3);
    end
    if (M(i,1) == 23907)              % interprocessor interrupts 
        M_to_file(b,6)=M(i,3);
    end
    if (M(i,1) == 23836)              % total intercepts
        M_to_file(b,7)=M(i,3);
	b = b + 1;
    end
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_hyperv_vp_interrupts.csv M_to_file;