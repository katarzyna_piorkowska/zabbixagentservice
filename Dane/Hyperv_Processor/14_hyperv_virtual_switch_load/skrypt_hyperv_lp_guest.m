# Host Test2
filename = "history.csv";
M = csvread(filename);
M_to_file = [];
M_to_file2 = [];
M_to_file4 = [];

% Logical processor
HypervGuestRunTime_LP_total = [];
HypervHypervisorRunTime_LP_total = [];
HypervGuestRunTime_LP_0 = [];
HypervHypervisorRunTime_LP_0 = [];
HypervGuestRunTime_LP_1 = [];
HypervHypervisorRunTime_LP_1 = [];
HypervGuestRunTime_LP_2 = [];
HypervHypervisorRunTime_LP_2 = [];
HypervGuestRunTime_LP_3 = [];
HypervHypervisorRunTime_LP_3 = [];
HypervGuestRunTime_LP_4 = [];
HypervHypervisorRunTime_LP_4 = [];
HypervGuestRunTime_LP_5 = [];
HypervHypervisorRunTime_LP_5 = [];
HypervGuestRunTime_LP_6 = [];
HypervHypervisorRunTime_LP_6 = [];
HypervGuestRunTime_LP_7 = [];
HypervHypervisorRunTime_LP_7 = [];
HypervTotalRunTime_LP = [];
HyperVTotalInterrupts_LP_total = [];
HyperVTotalInterrupts_LP_0 = [];
HyperVTotalInterrupts_LP_1 = [];
HyperVTotalInterrupts_LP_2 = [];
HyperVTotalInterrupts_LP_3 = [];
HyperVTotalInterrupts_LP_4 = [];
HyperVTotalInterrupts_LP_5 = [];
HyperVTotalInterrupts_LP_6 = [];
HyperVTotalInterrupts_LP_7 = [];
HypervHardwareInterrupts_LP = [];
HypervSchedulerInterrupts_LP = [];
HypervTimerInterrupts_LP = [];
HypervInterProcessorInterrupts_LP = [];
HypervContextSwitches_LP_total = [];
HypervContextSwitches_LP_0 = [];
HypervContextSwitches_LP_1 = [];
HypervContextSwitches_LP_2 = [];
HypervContextSwitches_LP_3 = [];
HypervContextSwitches_LP_4 = [];
HypervContextSwitches_LP_5 = [];
HypervContextSwitches_LP_6 = [];
HypervContextSwitches_LP_7 = [];
HypervMonitorTransitionCost_LP = [];

b = 1;
for i=1:rows(M)

    % LP guest run time
    if (M(i,1) == 24242)
        M_to_file(b,1)=M(i,2);
        M_to_file(b,2)=M(i,3);
    end
    if (M(i,1) == 24243)
        M_to_file(b,3)=M(i,3);
    end  
    if (M(i,1) == 24244)
        M_to_file(b,4)=M(i,3);
    end 
    if (M(i,1) == 24245)
        M_to_file(b,5)=M(i,3);
    end 
    if (M(i,1) == 24246)
        M_to_file(b,6)=M(i,3);
    end 
    if (M(i,1) == 24247)
        M_to_file(b,7)=M(i,3);
    end 
    if (M(i,1) == 24248)
        M_to_file(b,8)=M(i,3);
    end 
    if (M(i,1) == 24249)
        M_to_file(b,9)=M(i,3);
    end 
    if (M(i,1) == 24250)
        M_to_file(b,10)=M(i,3);
        b = b + 1;
    end 
end

a = 0;
for i=1:rows(M_to_file)
    M_to_file(i,1)=a;
    a = a + 10;
end

save history_hyperv_lp_guest_run_time.csv M_to_file;

