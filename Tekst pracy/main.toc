\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {polish}{}
\contentsline {section}{\numberline {1\relax .\kern .5em }Wst\IeC {\k e}p}{8}{section.1}
\contentsline {subsection}{\numberline {1.1\relax .\kern .5em }Cel oraz zakres pracy}{8}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2\relax .\kern .5em }Uk\IeC {\l }ad pracy}{9}{subsection.1.2}
\contentsline {section}{\numberline {2\relax .\kern .5em }Monitorowanie system\IeC {\'o}w}{10}{section.2}
\contentsline {subsection}{\numberline {2.1\relax .\kern .5em }Przegl\IeC {\k a}d literatury i dost\IeC {\k e}pnych rozwi\IeC {\k a}za\IeC {\'n}}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2\relax .\kern .5em }Wybrane technologie monitorowania system\IeC {\'o}w operacyjnych}{13}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1\relax .\kern .5em }Liczniki wydajno\IeC {\'s}ci systemu Windows}{14}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2\relax .\kern .5em }Monitor wydajno\IeC {\'s}ci}{14}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3\relax .\kern .5em }Oprogramowanie Zabbix}{15}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4\relax .\kern .5em }Sformu\IeC {\l }owanie zadania}{17}{subsection.2.4}
\contentsline {section}{\numberline {3\relax .\kern .5em }Zabbix agent}{19}{section.3}
\contentsline {subsection}{\numberline {3.1\relax .\kern .5em }Funkcje programu}{19}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2\relax .\kern .5em }Architektura}{19}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3\relax .\kern .5em }Model danych}{23}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4\relax .\kern .5em }Proces tworzenia oprogramowania}{24}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1\relax .\kern .5em }Wersja 1.1: Podstawowe modu\IeC {\l }y }{24}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2\relax .\kern .5em }Wersja 2.1: Przesy\IeC {\l }anie danych mi\IeC {\k e}dzy bazami}{25}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3\relax .\kern .5em }Wersja 2.2: Przetwarzanie proces\IeC {\'o}w}{26}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4\relax .\kern .5em }Wersja 2.3: Perfmon\_module}{26}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5\relax .\kern .5em }Wersja 3.1: Konfigurator agenta}{26}{subsubsection.3.4.5}
\contentsline {section}{\numberline {4\relax .\kern .5em }Badania}{28}{section.4}
\contentsline {subsection}{\numberline {4.1\relax .\kern .5em }Konfiguracja agenta Zabbixa}{28}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1\relax .\kern .5em }Wyb\IeC {\'o}r wykorzystywanych licznik\IeC {\'o}w}{28}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2\relax .\kern .5em }Symulowanie obci\IeC {\k a}\IeC {\.z}enia system\IeC {\'o}w}{30}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3\relax .\kern .5em }Dob\IeC {\'o}r warto\IeC {\'s}ci prog\IeC {\'o}w alarmowych}{31}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4\relax .\kern .5em }Wnioski}{33}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2\relax .\kern .5em }Weryfikacja dzia\IeC {\l }ania Monitora Wydajno\IeC {\'s}ci}{34}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1\relax .\kern .5em }Opis problemu}{34}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2\relax .\kern .5em }Rozwi\IeC {\k a}zanie}{36}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3\relax .\kern .5em }Monitoring zasob\IeC {\'o}w gospodarza i go\IeC {\'s}ci w \IeC {\'s}rodowisku zwirtualizowanym}{36}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1\relax .\kern .5em }Procesor}{39}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2\relax .\kern .5em }Pami\IeC {\k e}\IeC {\'c}}{47}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3\relax .\kern .5em }Dysk}{54}{subsubsection.4.3.3}
\contentsline {subsection}{\numberline {4.4\relax .\kern .5em }Badanie obci\IeC {\k a}\IeC {\.z}enia na gospodarzach \IeC {\'s}rodowiska zwirtualizowanego}{59}{subsection.4.4}
\contentsline {section}{\numberline {5\relax .\kern .5em }Podsumowanie}{64}{section.5}
\contentsline {section}{Bibliografia}{66}{section*.2}
\contentsline {section}{Wykaz symboli i skr\IeC {\'o}t\IeC {\'o}w}{68}{section*.3}
\contentsline {section}{Spis rysunk\IeC {\'o}w}{68}{section*.4}
\contentsline {section}{Spis tabel}{69}{section*.5}
\contentsline {section}{Spis za\IeC {\l }\IeC {\k a}cznik\IeC {\'o}w}{69}{section*.6}
