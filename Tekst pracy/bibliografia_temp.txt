Bibliografia

[1] https://docs.microsoft.com/en-us/windows/win32/perfctrs/performance-counters-portal

[2] https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-perf

[3] https://www.zabbix.com/documentation

[4] http://www.appadmintools.com/documents/windows-performance-counters-explained/

[5] https://www.groovypost.com/howto/troubleshoot-PC-using-performance-monitor-detailed-guide/

[6] https://www.poweradmin.com/blog/pages-per-second-counters/

[7] https://techcommunity.microsoft.com/t5/Ask-The-Performance-Team/The-Basics-of-Page-Faults/ba-p/373120

[8] http://woshub.com/huge-memory-usage-non-paged-pool-windows/

[9] https://www.concurrency.com/blog/september-2019/diagnosing-disk-performance-issues

[10] https://www.jam-software.com/heavyload/

[11] https://gallery.technet.microsoft.com/DiskSpd-a-robust-storage-6cd2f223

[12] https://azure.microsoft.com/pl-pl/overview/what-is-virtualization/

[13] https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/reference/hyper-v-architecture

[14] Windows Server 2008 Hyper-V: Insiders Guide to Microsoft's Hypervisor By John Kelbley, Mike Sterling, Allen Stewart (pages 4-7)

[15] https://blogs.msdn.microsoft.com/virtual_pc_guy/2008/02/28/hyper-v-virtual-machine-cpu-usage-and-task-manager/

[16] https://www.ibm.com/support/knowledgecenter/TI0002C/p8hat/p8hat_virtualproc.htm

[17] https://blogs.msdn.microsoft.com/tvoellm/2008/05/09/hyper-v-performance-counters-part-three-of-many-hyper-v-hypervisor-logical-processors-counter-set/

[18] https://blogs.msdn.microsoft.com/tvoellm/2008/05/12/hyper-v-performance-counters-part-four-of-many-hyper-v-hypervisor-virtual-processor-and-hyper-v-hypervisor-root-virtual-processor-counter-set/

[19] https://www.sciencedirect.com/topics/computer-science/symmetric-multi-processor

[20] https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/manage/manage-hyper-v-scheduler-types

[21] https://blogs.technet.microsoft.com/neales/2016/10/24/hyper-v-performance-cpu/

[22] https://docs.vmware.com/en/VMware-Workstation-Pro/12.0/com.vmware.ws.using.doc/GUID-9745D560-9243-4262-A585-D709D52B1349.html

[23] http://www.tmurgent.com/WhitePapers/ProcessorAffinity.pdf
